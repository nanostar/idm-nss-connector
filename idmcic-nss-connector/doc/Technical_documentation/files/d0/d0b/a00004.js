var a00004 =
[
    [ "GetCog", "d0/d0b/a00004.html#a358ae0181c71dea7c04823edfa2c2ca6", null ],
    [ "GetConsumedPower", "d0/d0b/a00004.html#a703447ba451eb8e7ac000a00ec321f4a", null ],
    [ "GetDissipatedPower", "d0/d0b/a00004.html#aa987e468969e2280c121e1d5082208e0", null ],
    [ "GetDutyCycleConsumedPower", "d0/d0b/a00004.html#a6686b312c9009081185012f1def3db5e", null ],
    [ "GetDutyCycleDissipatedPower", "d0/d0b/a00004.html#a243af97953148f31ab13e2e1d3dd5e52", null ],
    [ "GetInertiaMatrix", "d0/d0b/a00004.html#af4ec5eff98ae3e07d6de51b06ae3274b", null ],
    [ "GetMass", "d0/d0b/a00004.html#a90501de4d45f16a7ec90a6bd5c7578fa", null ],
    [ "GetObject", "d0/d0b/a00004.html#ab3f1362bc2beb64455806a96fa658570", null ],
    [ "HasDefaultModel", "d0/d0b/a00004.html#a90494adba4b91988b6d718b3f3ce6396", null ],
    [ "IdmFilesList", "d0/d0b/a00004.html#af9903b4ced7d457e544ab655c0e0508c", null ],
    [ "LoadIdmModel", "d0/d0b/a00004.html#ae620a7342229bbfd00414c7cbb22c047", null ]
];