var a00003 =
[
    [ "GetIdmStudy", "d1/d7c/a00003.html#adb0db99d410fd89b7c0cf48f7e39accc", null ],
    [ "IsStarted", "d1/d7c/a00003.html#a1f36376962e7dce3feb7edcc214784df", null ],
    [ "LoadIdmModel", "d1/d7c/a00003.html#aaf6ad3877ede997bf9a9d0a0969f655b", null ],
    [ "Start", "d1/d7c/a00003.html#afa236f002f9de3c027864847f013692f", null ],
    [ "Stop", "d1/d7c/a00003.html#ab9aae1ab44fea18e1106c8ec2fdbab95", null ],
    [ "DefaultStudy", "d1/d7c/a00003.html#a678875c684cc1e6a4ff4ebc65770ab06", null ],
    [ "ExposedIdmFiles", "d1/d7c/a00003.html#a63feeddf7a48b6c58b7f911668982983", null ],
    [ "ModelsSessions", "d1/d7c/a00003.html#aac3a58038c9430209b1f49851b9f398e", null ],
    [ "ModelsSessionsLifeTime", "d1/d7c/a00003.html#a179b1af0ddc64017e0716fedee91ea73", null ],
    [ "Port", "d1/d7c/a00003.html#ae5d575de343ad160e2a6a62faa09d722", null ],
    [ "RestrictToExposedModels", "d1/d7c/a00003.html#a3dc5c8c412e6b611e07d52bca1ee0f5f", null ],
    [ "PropertyChanged", "d1/d7c/a00003.html#a45bb05a151d19722ea8d755a16514452", null ]
];