var hierarchy =
[
    [ "ApiController", null, [
      [ "IdmCicRestApi.Server.IdmCicWebServiceController", "d0/d0b/a00004.html", null ]
    ] ],
    [ "IdmCicRestApi.Server.Data.ExposedFileData", "d8/d84/a00001.html", null ],
    [ "INotifyCollectionChanged", null, [
      [ "IdmCicRestApi.Server.Data.ExposedFileDataCollection", "d7/d46/a00002.html", null ],
      [ "IdmCicRestApi.Server.Data.ModelSessionDataCollection", "d4/dee/a00008.html", null ]
    ] ],
    [ "INotifyPropertyChanged", null, [
      [ "IdmCicRestApi.Server.IdmCicHttpServer", "d1/d7c/a00003.html", null ]
    ] ],
    [ "KeyedCollection", null, [
      [ "IdmCicRestApi.Server.Data.ExposedFileDataCollection", "d7/d46/a00002.html", null ],
      [ "IdmCicRestApi.Server.Data.ModelSessionDataCollection", "d4/dee/a00008.html", null ]
    ] ],
    [ "IdmCicRestApi.Server.Data.ModelSessionData", "de/d5e/a00007.html", null ],
    [ "ValidationRule", null, [
      [ "IdmCicRestApi.Helpers.IntegerValidationRule", "dd/dad/a00005.html", null ]
    ] ],
    [ "Window", null, [
      [ "IdmCicRestApi.MainWindow", "de/d21/a00006.html", null ]
    ] ]
];