import { Component, Input, Output, EventEmitter } from '@angular/core';
import { API_ATTRS } from '../endpoints.conffig';
import { ApiService } from '../api.service';

@Component({
    selector: 'app-display-panel',
    templateUrl: './display-panel.component.html',
    styleUrls: ['./display-panel.component.scss']
})
export class DisplayPanelComponent {

    @Input('url') url = ''
    @Input('configurationId') configurationId = ''
    @Input('systemModeId') systemModeId = ''
    @Input('sessionId') sessionId = ''
    @Input('iEqMar') iEqMar = ''
    @Input('iElMar') iElMar = ''
    @Input('iSysProMar') iSysProMar = ''
    @Input('iEqMarPow') iEqMarPow = ''
    @Input('iElMarPow') iElMarPow = ''

    @Output() onError = new EventEmitter<String>();

    retrievings = {}

    values = {
        getMass: '',
        getConsumedPower: '',
        getCog: ['', '', ''],
        getDissipatedPower: '',
        getDutyCycleConsumedPower: '',
        getInertiaMatrix: [
            ['', '', ''],
            ['', '', ''],
            ['', '', '']
        ],
        getDutyCycleDissipatedPower: ''
    }

    constructor(
        private api: ApiService
    ) {
        for (const attr in API_ATTRS) {
            this.retrievings[attr] = false
        }
        this.retrievings['all'] = false
    }

    /**
     * Send string error to the parent to display it
     * @param error string error
     */
    sendError(error = '') {
        this.onError.emit(error)
    }

    /**
     * Reset the error displayed by the parent component
     */
    resetError() {
        this.onError.emit('')
    }

    /**
     * Execute API request depending on the attr see : API_ATTR const in imports for more details
     * @param attr
     */
    exec(attr = '', then = null) {
        if(this.retrievings[attr]) return;
        if(this.isDisabled(attr)) return;
        this.resetError()
        this.retrievings[attr] = true

        let params = this.getParams(attr)

        this.api.get(`${this.url.replace(/\/$/, "")}/${attr}?${
            // Excluding empty [null] values from url
            params.filter(i => (!!i)).map(param => `${param.name}=${param.value}`).join('&')
            // format array to : ?a=1&b=2
        }`).then(resp => {
            this.retrievings[attr] = false
            this.values[attr] = resp
            if(then) then()
        }).catch(e => {
            this.retrievings[attr] = false
            if(then) then()
        })
    }

    /**
     * Execute all attributes retrieving syncronously
     */
    execAll() {
        this.retrievings['all'] = true
        let tasks = []
        Object.keys(API_ATTRS).forEach(key => {
            // Check if not already launched and all parameters required are ok
            if(!this.isDisabled(key) && !this.isLoading(key)) tasks.push(key)
        })
        this.execTask(tasks, () => {
            this.retrievings['all'] = false
        })
    }

    /**
     * Execute array of tasks sync
     * @param tasks Array of keys attributes
     * @param end function executed when all tasks are done
     */
    execTask(tasks, end) {
        let task = tasks.shift()
        if(task) {
            this.exec(task, () => {
                this.execTask(tasks, end)
            })
        } else {
            if(end) end()
        }
    }

    /**
     * Get the parameters to create the URL to do the GET Request
     * @param attr name of the API function called ei: getMass
     */
    getParams(attr = '') {
        // Mapping parameters from API_ATTRS to build the URL query
        let params = API_ATTRS[attr].map(item => {
            let _param =  {
                name: item.param,
                value: this[item.param]
            }
            // Mapping Pow checkboxes to params without 'Pow' sufix
            if(_param.name === 'iEqMarPow') _param.name = 'iEqMar'
            if(_param.name === 'iElMarPow') _param.name = 'iElMar'
            // Return null if param value is empty
            if(_param.value === '') return null;
            return _param;
        })
        return params
    }

    /**
     * Check if the request is not already sent and not yet arrived
     * @param attr name of the function ei: getMass
     */
    isLoading(attr = '') {
        return this.retrievings[attr]
    }

    isDisabled(attr = '') {
        let _params = API_ATTRS[attr]
        let disabled = false
        _params.forEach(item => {
            if(item.required && !this[item.param]) disabled = true
        })
        return (disabled ? 'disabled' : '')
    }

    /**
     * Simple function to select the content of the table #html_table to copy it to clipboard
     */
    copyHtml() {
        let el = document.getElementById('html_table')
        let range, sel;
        if (document.createRange && window.getSelection) {
            range = document.createRange()
            sel = window.getSelection()
            sel.removeAllRanges()
            try {
                range.selectNodeContents(el);
                sel.addRange(range);
            } catch (e) {
                range.selectNode(el);
                sel.addRange(range);
            }
            document.execCommand('copy')
            sel.removeAllRanges()
        }
    }
}