import { Component, AfterViewInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Input } from '@angular/core';
import { LoggerService } from '../logger.service';

@Component({
    selector: 'app-main-panel',
    templateUrl: './main-panel.component.html',
    styleUrls: ['./main-panel.component.scss'],
    providers:  [ ApiService, LoggerService ]
})

export class MainPanelComponent implements AfterViewInit {

    @Input() url = ''
    @Input() path = ''

    sessionId: any = ''
    models = []

    error = ''
    container: HTMLElement

    confId = ''
    sysmodeId= ''
    configurations = []
    sysmodes = []
    configsLoaded = false
    modesLoaded = false
    modelsLoading = false
    modelLoading = false

    iEqMar = true
    iElMar = true
    iSysProMar = true

    iEqMarPow = true
    iElMarPow = true


    constructor(
        private api: ApiService,
        private logger: LoggerService
    ) {
        /**
         * Scroll down at each new log
         */
        this.logger.last.subscribe(() => {
            setTimeout(() => {
                this.container.scrollTop = this.container.scrollHeight
            }, 1)
        })

        if(localStorage.getItem('api_url')) {
            this.url = localStorage.getItem('api_url')
        }
    }

	/**
	* Returns current logs contained in the private logger
	*/
	getLogs() {
		return this.logger.logs
	}
	
    /**
     * Triggers after the view is initialized.
     * Used to initialize the auto scroll to bottom of the logs
     */
    ngAfterViewInit() {
        this.container = document.getElementById("logs")
        this.container.scrollTop = this.container.scrollHeight
    }

    /**
     * Click on Retrieve models button
     */
    onRetrieveModels() {
        this.error = ''
        this.modelsLoading = true
        localStorage.setItem('api_url', this.url)
        this.api.getModels(this.url).then(resp => {
            // Swapping {key: val} to : [{key: key, val: val}]
            this.models = Object.keys(resp).map(path => ({
                path: path,
                label: resp[path]
            }))
            this.modelsLoading = false
        }).catch(e => {
            this.error = 'An error has occured. Check the logs for more precision.'
            this.modelsLoading = false
        })
    }

    /**
     * Click on open model button
     */
    openModel() {
        this.modelLoading = true
        this.reset()
        this.api.getModel(this.url, this.path).then(sessionId => {
            this.sessionId = sessionId
            // Load default model on open
            this.loadMainModel()
            this.modelLoading = false
        }).catch(e => {
            this.error = 'An error has occured. Check the logs for more precision.'
            this.modelLoading = false
        })
    }

    /**
     * Function passed to children components to display error
     * @param error the String error to display
     */
    onError(error = '') {
        this.error = error
    }

    /**
     * This function is called after the retrieving of the selected model
     * to load the References configuration to load sub-configurations
     * and sub-system-modes
     */
    loadMainModel() {
        this.reset()
        this.api.getObject(this.url, this.sessionId).then(r => {
            // Load all configurations to get the name to fill select
            let configs = r['Configurations']
            this.configurations = [{value: '', item: {Name: 'Reference'}}]

            this.loadConfigs(configs)
            // Load all sysmodes to get the name to fill select
            let sysmodes = r['SystemModes']
            if(sysmodes.length) {
                this.sysmodes = []
                this.loadModes(sysmodes)
            } else {
                this.sysmodes = [{value: '', item: {Name: '- No system modes -'}}]
                this.modesLoaded = true
            }
        })
    }

    /**
     * Function to reset the displays of errors
     * used when the model change to hide previews datas
     */
    reset() {
        this.error = ''
        this.modesLoaded = false
        this.configsLoaded = false
        this.configurations = []
        this.sysmodes = []
    }

    /**
     * Return if the subpanel can be displayed or not
     * here, when the configs and sysmodes are lodaded
     */
    isSubPanel() {
        return (this.configsLoaded && this.modesLoaded)
    }

    /**
     * Check if none of the path in list is the current typed path
     */
    isNotPathInList() {
        return this.models.find(m => m.path === this.path) ? '' : 'selected'
    }

    /**
     * Check if a path is the current typed path
     */
    isPath(path) {
        return (this.path === path) ? 'selected' : ''
    }

    /**
     * Take the array of configs and load configs one by one to get the name
     * @param configs array of objects
     */
    loadConfigs(configs: []) {
        let loaded = 0
        if(!configs.length) {
            this.configsLoaded = true
            return;
        }
        configs.slice(0).forEach(config => {
            // Sync the end of all async call to retrieve configs
            let next = () => { this.configsLoaded = (++loaded === configs.length) }
            this.api.getObject(this.url, this.sessionId, config['FullId']).then(r => {
                if(!r['Name']) r['Name'] = `Configuration ${r['Id']}`
                this.configurations.push({ value: r['Id'], item: r })
                next()
            }).catch(e => next)
        });
    }

    /**
     * Return true or false if this mode is selected
     * @param mode
     */
    isSelectedMode(mode) {
        return this.sysmodeId === mode ? 'selected' : '';
    }

    /**
     * Take the array of modes and load modes one by one to get the name
     * @param modes array of objects
     */
    loadModes(modes: []) {
        let loaded = 0
        modes.slice(0).forEach(mode => {
            // Sync the end of all async call to retrieve sysmodes
            let next = () => {
                this.modesLoaded = (++loaded === modes.length)
                if(this.modesLoaded && this.sysmodes[0]) this.changeMode(this.sysmodes[0].value)
            }
            this.api.getObject(this.url, this.sessionId, mode['FullId']).then(r => {
                if(!r['Name']) r['Name'] = `System mode ${r['Id']}`
                this.sysmodes.push({ value: r['Id'], item: r })
                next()
            }).catch(e => next)
        });
    }

    /**
     * Setter for the attribute model (the currently selected model)
     * @param val
     */
    changeModel(val = '') {
        this.path = val
    }

    /**
     * Setter for the attribute configuration (the currently selected configuration)
     * @param val
     */
    changeConf(val = '') {
        this.confId = val
    }

    /**
     * Setter for the attribute mode (the currently selected mode)
     * @param val
     */
    changeMode(val = '') {
        this.sysmodeId = val
    }

}
