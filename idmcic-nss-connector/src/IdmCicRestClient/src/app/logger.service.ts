import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class LoggerService {

    logs: Log[] = [];
    last: Observable<Log>;
    $observer: any = null;

    constructor() {
        this.last = new Observable<Log>(observer => {
            this.$observer = observer
        })
    }

    /**
     * Add log to queue and emit it
     * @param log type Log
     */
    addLog(log: Log) {
        this.logs.push(log)
        this.$observer.next(log)
    }

    /**
     * Thoose functions changes the type of the log (for color purpose only)
     * @param msg
     */
    log(msg: any)   { this.addLog(new Log({type: 'log', msg: msg})) }
    error(msg: any) { this.addLog(new Log({type: 'error', msg: msg})) }
    warn(msg: any)  { this.addLog(new Log({type: 'warn', msg: msg})) }
    info(msg: any)  { this.addLog(new Log({type: 'info', msg: msg})) }
}

export class Log {
    msg: any = ''
    type: 'log'

    constructor(props = {}) {
        Object.assign(this, props)
    }

    toString() {
        return (typeof this.msg === 'object' ? JSON.stringify(this.msg) : this.msg)
    }
}
