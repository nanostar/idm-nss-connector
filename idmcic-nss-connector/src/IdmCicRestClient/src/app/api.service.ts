import { Injectable } from '@angular/core';
import { LoggerService } from './logger.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { FILES_LIST, IDM_MODEL, MODEL_PATH, GET_OBJECT, SESSION_ID, FULL_ID } from './endpoints.conffig'

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(
        private logger: LoggerService,
        private http: HttpClient
    ) { }

    /**
     * Generic function get() to perform a GET call and return a Promise
     * @param url String : the url to call
     * @param headers Object : the headers (not used)
     */
    get(url = '', headers = null) {
        let httpOptions = headers ? { headers: new HttpHeaders(headers) } : {}
        this.logger.info(`Retrieving datas from : ${url}`)
        let time = (new Date).getTime()
        return new Promise((resolve, reject) => {
            this.http.get(url, httpOptions).subscribe(datas => {
                this.logger.log(datas)
                this.logger.info('Time elapsed : ' + ((new Date).getTime() - time) + 'ms')
                resolve(datas)
            }, error => {
                this.logger.error(error)
                this.logger.info('Time elapsed : ' + ((new Date).getTime() - time) + 'ms')
                reject(error)
            })
        })
    }

    /**
     * Get the list of the models
     * @param url the base url to call
     */
    getModels(url = '') {
        return this.get(`${url.replace(/\/$/, "")}/${FILES_LIST}`)
    }

    /**
     * Get the details of the model
     * @param url  the base url to call
     * @param path the path of the model
     */
    getModel(url = '', path = '') {
        return this.get(`${url.replace(/\/$/, "")}/${IDM_MODEL}?${MODEL_PATH}=${path}`)
    }

    /**
     * Get the details on a configuration or a system mode
     * @param url the base url to call
     * @param sessionId current id of session
     * @param fullId the optionnal fullId
     */
    getObject(url = '', sessionId = '', fullId = null) {
        let _url = `${url.replace(/\/$/, "")}/${GET_OBJECT}?${SESSION_ID}=${sessionId}`
        if(fullId) _url += `&${FULL_ID}=${fullId}`
        return this.get(_url)
    }
}
