export const FILES_LIST = 'idmFilesList';

export const IDM_MODEL = 'loadIdmModel';

export const MODEL_PATH = 'modelpath';

export const GET_OBJECT = 'getObject';

export const SESSION_ID = 'sessionId';

export const FULL_ID = 'fullId';

export const API_ATTRS = {
    'getMass': [
        { param: 'sessionId', required: true },
        { param: 'iEqMar', required: false },
        { param: 'iElMar', required: false },
        { param: 'iSysProMar', required: false },
        { param: 'configurationId', required: false }
    ],
    'getCog': [
        { param: 'sessionId', required: true },
        { param: 'iEqMar', required: false },
        { param: 'iElMar', required: false },
        { param: 'iSysProMar', required: false },
        { param: 'configurationId', required: false }
    ],
    'getInertiaMatrix': [
        { param: 'sessionId', required: true },
        { param: 'iEqMar', required: false },
        { param: 'iElMar', required: false },
        { param: 'iSysProMar', required: false },
        { param: 'configurationId', required: false }
    ],
    'getConsumedPower': [
        { param: 'sessionId', required: true },
        { param: 'iEqMarPow', required: false },
        { param: 'iElMarPow', required: false },
        { param: 'configurationId', required: false },
        { param: 'systemModeId', required: true }
    ],
    'getDissipatedPower': [
        { param: 'sessionId', required: true },
        { param: 'iEqMarPow', required: false },
        { param: 'iElMarPow', required: false },
        { param: 'configurationId', required: false },
        { param: 'systemModeId', required: true }
    ],
    'getDutyCycleConsumedPower': [
        { param: 'sessionId', required: true },
        { param: 'iEqMarPow', required: false },
        { param: 'iElMarPow', required: false },
        { param: 'configurationId', required: false }
    ],
    'getDutyCycleDissipatedPower': [
        { param: 'sessionId', required: true },
        { param: 'iEqMarPow', required: false },
        { param: 'iElMarPow', required: false },
        { param: 'configurationId', required: false }
    ],
}
