﻿// License header goes here.
using IdmCicRestApi.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace UnitTests
{
    /// <summary>
    /// This test class holds unit tests that help to test the IDM-CIC HTTP server configuration.
    /// </summary>
    [TestClass]
    public class ServerConfigurationTest
    {
        /// <summary>
        /// Ensure that the IDM-CIC HTTP server is a singleton which instance is initialized.
        /// </summary>
        [TestMethod]
        public void TestSingleton()
        {
            Assert.IsNotNull(IdmCicHttpServer.Instance);
        }

        /// <summary>
        /// Ensures that the server can start, stop and raise exceptions when trying to start the server while it's 
        /// already started or when trying to stop the server while it's not started.
        /// </summary>
        [TestMethod]
        public void TestServerState()
        {
            // Ensure the server is stopped before entering the test
            if (IdmCicHttpServer.Instance.IsStarted())
            {
                IdmCicHttpServer.Instance.Stop();
            }

            // Test starting the server and check that it's effectivelly specified as opened.
            TestsHelper.StartServerWithDefaultPort();
            Assert.IsTrue(IdmCicHttpServer.Instance.IsStarted());

            // Ensure that now it's started, calling the "Start" function throws an exception
            TestsHelper.AssertThrowException(delegate ()
            {
                IdmCicHttpServer.Instance.Start();
            });

            // Test stoping the server and check that it's effectivelly specified as closed.
            IdmCicHttpServer.Instance.Stop();
            Assert.IsFalse(IdmCicHttpServer.Instance.IsStarted());

            // Ensure that now it's closed, calling the "Stop" function throws an exception
            TestsHelper.AssertThrowException(delegate ()
            {
                IdmCicHttpServer.Instance.Stop();
            });
        }
        
        [TestMethod]
        public void TestPropertiesErrors()
        {
            // Ensure the server is stopped before entering the test
            if (IdmCicHttpServer.Instance.IsStarted())
            {
                IdmCicHttpServer.Instance.Stop();
            }

            // Ensure the port can only be an integer grater than 0.
            TestsHelper.AssertThrowException(delegate ()
            {
                IdmCicHttpServer.Instance.Port = -1;
            });

            TestsHelper.AssertThrowException(delegate ()
            {
                IdmCicHttpServer.Instance.Port = 0;
            });

            // Ensure the models sessions life time can only be an integer grater than 0.
            TestsHelper.AssertThrowException(delegate ()
            {
                IdmCicHttpServer.Instance.ModelsSessionsLifeTime = -1;
            });

            TestsHelper.AssertThrowException(delegate ()
            {
                IdmCicHttpServer.Instance.ModelsSessionsLifeTime = 0;
            });
            
            // Starts the server
            TestsHelper.StartServerWithDefaultPort();

            // Ensure that changing the port when the server is started throws an exception
            TestsHelper.AssertThrowException(delegate ()
            {
                IdmCicHttpServer.Instance.Port = TestsHelper.DEFAULT_SERVER_PORT + 1;
            });

            // Stop the server
            IdmCicHttpServer.Instance.Stop();
        }

        /// <summary>
        /// Test that exposed data files of the server can be set through a dictionary and that the collection is cleared when using the function.
        /// </summary>
        [TestMethod]
        public void TestExposedFilesFromDictionary()
        {
            // Ensure that 3 items are added to the exposed models collection
            Dictionary<string,string> exposedFiles = new Dictionary<string,string>() { { "C:\\file1.idm", "File 1" }, { "C:\\file2.idm", "File 2" }, { "C:\\file3.idm", "File 3" } };
            IdmCicHttpServer.Instance.ExposedIdmFiles.FromDictionary(exposedFiles);
            Assert.AreEqual(3, IdmCicHttpServer.Instance.ExposedIdmFiles.Count);

            // Check consistency on the server size according to the dictionary passed to the function
            foreach (KeyValuePair<string, string> kvp in exposedFiles)
            {
                Assert.IsTrue(IdmCicHttpServer.Instance.ExposedIdmFiles.Contains(kvp.Key));
                Assert.AreEqual(IdmCicHttpServer.Instance.ExposedIdmFiles[kvp.Key].FilePath, kvp.Key);
                Assert.AreEqual(IdmCicHttpServer.Instance.ExposedIdmFiles[kvp.Key].CustomName, kvp.Value);
            }

            // Ensure that 2 items are added to the exposed models collection (should be reinitialised)
            exposedFiles = new Dictionary<string, string>() { { "C:\\file4.idm", "File 4" }, { "C:\\file5.idm", "File 5" } };
            IdmCicHttpServer.Instance.ExposedIdmFiles.FromDictionary(exposedFiles);
            Assert.AreEqual(2, IdmCicHttpServer.Instance.ExposedIdmFiles.Count);

            // Check consistency on the server size according to the dictionary passed to the function
            foreach (KeyValuePair<string, string> kvp in exposedFiles)
            {
                Assert.IsTrue(IdmCicHttpServer.Instance.ExposedIdmFiles.Contains(kvp.Key));
                Assert.AreEqual(IdmCicHttpServer.Instance.ExposedIdmFiles[kvp.Key].FilePath, kvp.Key);
                Assert.AreEqual(IdmCicHttpServer.Instance.ExposedIdmFiles[kvp.Key].CustomName, kvp.Value);
            }
        }
    }
}
