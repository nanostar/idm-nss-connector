﻿// License header goes here.
using IdmCic.API.Model;
using IdmCic.API.Model.Configurations;
using IdmCic.API.Model.Mainsystem;
using IdmCic.API.Model.Power;
using IdmCic.API.Utils.Calculation.MCI;
using IdmCic.API.Utils.Calculation.Power;
using IdmCicRestApi.Server;
using IdmCicRestApi.Server.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;

namespace UnitTests
{
    /// <summary>
    /// These test class holds unit tests that help to test calls to the IDM-CIC Rest API.
    /// </summary>
    [TestClass]
    public class ApiCallsTest
    {
        #region Test class features

        /// <summary>
        /// The test context that gives information about the active tests suite and its features
        ///</summary>
        private TestContext testContextInstance;
        
        /// <summary>
        /// Get or set the test context that gives information about the active tests suite and its features
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Starts the IDM CIC HTTP server when this test class is instantiated.
        /// We can start the server only once for all this class test so API functions can be called.
        /// </summary>
        /// <param name="testContext">The current test context</param>
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            if (!IdmCicHttpServer.Instance.IsStarted())
            {
                TestsHelper.StartServerWithDefaultPort();
            }
        }

        /// <summary>
        /// Stops the IDM CIC HTTP server when this test class is released because not needed anymore.
        /// </summary>
        [ClassCleanup]
        public static void ClassCleanup()
        {
            if (IdmCicHttpServer.Instance.IsStarted())
            {
                IdmCicHttpServer.Instance.Stop();
            }
        }

        /// <summary>
        /// Cleans all server model sessions and exposed IDM files after each test.
        /// Ensures also that the server is not restricted to exposed models and sessions lifetime is long enough.
        /// </summary>
        [TestCleanup]
        public void TestCleanup()
        {
            IdmCicHttpServer.Instance.ModelsSessions.Clear();
            IdmCicHttpServer.Instance.ExposedIdmFiles.Clear();
            IdmCicHttpServer.Instance.RestrictToExposedModels = false;
            IdmCicHttpServer.Instance.ModelsSessionsLifeTime = 900;
        }

        #endregion

        /// <summary>
        /// Test of the IdmFilesList REST API function
        /// </summary>
        [TestMethod]
        public void TestIdmFilesList()
        {
            TestsHelper.IdmCicHttpResponse response;

            // Test exposed files when nothing is defined within the server
            response = TestsHelper.GetServerResponseWithStatusCheck("IdmFilesList", true);
            
            TestsHelper.AssertJsonObjectsEqual(IdmCicHttpServer.Instance.ExposedIdmFiles.ToDictionary(), response.StringValue);

            // Test exposed files with some exposed models
            IdmCicHttpServer.Instance.ExposedIdmFiles.Add(new ExposedFileData("C:\\MyFirstIdmFile.idm", "My first IDM file custom name"));
            IdmCicHttpServer.Instance.ExposedIdmFiles.Add(new ExposedFileData("C:\\MySecondIdmFile.idm", "My second IDM file custom name"));

            response = TestsHelper.GetServerResponseWithStatusCheck("IdmFilesList", true);
            
            TestsHelper.AssertJsonObjectsEqual(IdmCicHttpServer.Instance.ExposedIdmFiles.ToDictionary(), response.StringValue);
        }
        
        /// <summary>
        /// Test of the LoadModel REST API function.
        /// This test is performed for no restriction on exposed files.
        /// </summary>
        [TestMethod]
        public void TestLoadModel()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            TestsHelper.IdmCicHttpResponse response;

            // Try to load a valid model
            parameters["modelPath"] = TestsHelper.GetTestModelPath("CIC-SAT");
            response = TestsHelper.GetServerResponseWithStatusCheck("LoadIdmModel", true, parameters);
            
            Assert.IsNotNull(response.StringValue);
            string sessionId = JsonConvert.DeserializeObject<string>(response.StringValue);

            // Check there is effectively a session ID
            Assert.IsFalse(string.IsNullOrEmpty(sessionId));

            // Ensure the returned value can effectivelly be parsed as a GUID
            Guid testGuid = Guid.Parse(sessionId);

            // Ensure the session effectively exists within the server
            Assert.IsTrue(IdmCicHttpServer.Instance.ModelsSessions.Contains(sessionId));

            // Try to load an invalid model (should raise an error)
            parameters["modelPath"] = TestsHelper.GetTestModelPath("Invalid-Model");
            response = TestsHelper.GetServerResponseWithStatusCheck("LoadIdmModel", false, parameters);

            // Try to load a non existing file (should raise an error)
            parameters["modelPath"] = "C:\\FileNotFound.idm";
            response = TestsHelper.GetServerResponseWithStatusCheck("LoadIdmModel", false, parameters);
        }

        /// <summary>
        /// Test of the LoadModel REST API function.
        /// This test is performed for server restricted mode (restriction on exposed files).
        /// </summary>
        [TestMethod]
        public void TestLoadModelRestricted()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            TestsHelper.IdmCicHttpResponse response;

            // Add first file as exposed
            IdmCicHttpServer.Instance.ExposedIdmFiles.Add(new ExposedFileData(TestsHelper.GetTestModelPath("CIC-SAT"), "Test 1"));
            
            // The server is not restricted to exposed models (by the test class configuration). 
            // Load a model not exposed (should be successful)
            parameters["modelPath"] = TestsHelper.GetTestModelPath("Orbit-CIC-SAT");
            response = TestsHelper.GetServerResponseWithStatusCheck("LoadIdmModel", true, parameters);
            
            // Activate the server restriction mode
            IdmCicHttpServer.Instance.RestrictToExposedModels = true;

            // Load a model not exposed (should raise an error)
            response = TestsHelper.GetServerResponseWithStatusCheck("LoadIdmModel", false, parameters);

            // Load a model exposed (should be successful)
            parameters ["modelPath"] = TestsHelper.GetTestModelPath("CIC-SAT");
            response = TestsHelper.GetServerResponseWithStatusCheck("LoadIdmModel", true, parameters);
        }
        
        /// <summary>
        /// Test sessions life time to ensure sessions are effectivelly released by their timer.
        /// </summary>
        [TestMethod]
        public void TestSessionsLifeTime()
        {
            TestsHelper.IdmCicHttpResponse response;
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            // Set a small models sessions lifetime
            int sessionsLifeTimeToTest = 5;
            IdmCicHttpServer.Instance.ModelsSessionsLifeTime = sessionsLifeTimeToTest;

            // Load a test model and retrieve the session ID
            string testModelPath = TestsHelper.GetTestModelPath("CIC-SAT");
            parameters["modelPath"] = testModelPath;
            response = TestsHelper.GetServerResponseWithStatusCheck("LoadIdmModel", true, parameters);

            string sessionId = JsonConvert.DeserializeObject<string>(response.StringValue);
            Assert.IsTrue(IdmCicHttpServer.Instance.ModelsSessions.Contains(sessionId));

            // Configure parameters to call functions with this session ID
            parameters = new Dictionary<string, string>() { { "sessionId", sessionId } };

            // Try the get object method for the main system. The object should be retrieved.
            response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", true, parameters);

            // Check that the session timer was reinitialized by calling again the function many times with a small sleep 
            // to ensure the total time for all these calls is greater than the configured session life time.
            // Should always be successful
            for (int i = 0; i < 5; i++)
            {
                Thread.Sleep((sessionsLifeTimeToTest - 2) * 1000);
                Assert.IsTrue(IdmCicHttpServer.Instance.ModelsSessions.Contains(sessionId));
                response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", true, parameters);
            }

            // Wait for the session to terminate.
            // The session should not be stored anymore and calling an API function for this session should raise an error.
            Thread.Sleep((sessionsLifeTimeToTest +1) * 1000);
            Assert.IsFalse(IdmCicHttpServer.Instance.ModelsSessions.Contains(sessionId));
            response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", false, parameters);
        }

        /// <summary>
        /// Test the REST API functions for the default study.
        /// Ensure that if there is a default study, API functions can be called without the sessionId parameter.
        /// </summary>
        [TestMethod]
        public void TestDefaultStudy()
        {
            TestsHelper.IdmCicHttpResponse response;

            IdmCicHttpServer.Instance.DefaultStudy = null;
            
            // Check the server respond that it has a default model loaded
            response = TestsHelper.GetServerResponseWithStatusCheck("HasDefaultModel", true);
            Assert.IsFalse(JsonConvert.DeserializeObject<bool>(response.StringValue));

            // Check the server function  can't be called without session ID specified
            response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", false);


            // Set a small models sessions lifetime for the test needs
            int sessionsLifeTimeToTest = 10;
            IdmCicHttpServer.Instance.ModelsSessionsLifeTime = sessionsLifeTimeToTest;

            // Set a new study as default study with some custom properties
            IdmCicHttpServer.Instance.DefaultStudy = new IdmStudy();
            string projectNameToTest = "TestDefaultStudy project name";
            IdmCicHttpServer.Instance.DefaultStudy.MainSystem.ProjectName = projectNameToTest;

            // Check the server respond that it has a default model loaded
            response = TestsHelper.GetServerResponseWithStatusCheck("HasDefaultModel", true);
            Assert.IsTrue(JsonConvert.DeserializeObject<bool>(response.StringValue));

            testGetObject(IdmCicHttpServer.Instance.DefaultStudy, null);
            testGetObjectFullSerialization(IdmCicHttpServer.Instance.DefaultStudy, null);
            testMci(MciTest.Mass, IdmCicHttpServer.Instance.DefaultStudy, null);
            testMci(MciTest.Cog, IdmCicHttpServer.Instance.DefaultStudy, null);
            testMci(MciTest.InertiaMatrix, IdmCicHttpServer.Instance.DefaultStudy, null);
            testPower(PowerTest.ConsumedPower, IdmCicHttpServer.Instance.DefaultStudy, null);
            testPower(PowerTest.DissipatedPower, IdmCicHttpServer.Instance.DefaultStudy, null);
            testPower(PowerTest.DutyCycleConsumedPower, IdmCicHttpServer.Instance.DefaultStudy, null);
            testPower(PowerTest.DutyCycleDissipatedPower, IdmCicHttpServer.Instance.DefaultStudy, null);

            // Ensure The default study is not released by model sessions lifetime .
            Thread.Sleep((sessionsLifeTimeToTest + 1) * 1000);
            response = TestsHelper.GetServerResponseWithStatusCheck("HasDefaultModel", true);
            Assert.IsTrue(JsonConvert.DeserializeObject<bool>(response.StringValue));
            response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", true);

            // Test with a more complexe study
            IdmCicHttpServer.Instance.DefaultStudy = new IdmStudy(TestsHelper.GetTestModelPath("CIC-SAT"));

            testGetObject(IdmCicHttpServer.Instance.DefaultStudy, null);
            testGetObjectFullSerialization(IdmCicHttpServer.Instance.DefaultStudy, null);
            testMci(MciTest.Mass, IdmCicHttpServer.Instance.DefaultStudy, null);
            testMci(MciTest.Cog, IdmCicHttpServer.Instance.DefaultStudy, null);
            testMci(MciTest.InertiaMatrix, IdmCicHttpServer.Instance.DefaultStudy, null);
            testPower(PowerTest.ConsumedPower, IdmCicHttpServer.Instance.DefaultStudy, null);
            testPower(PowerTest.DissipatedPower, IdmCicHttpServer.Instance.DefaultStudy, null);
            testPower(PowerTest.DutyCycleConsumedPower, IdmCicHttpServer.Instance.DefaultStudy, null);
            testPower(PowerTest.DutyCycleDissipatedPower, IdmCicHttpServer.Instance.DefaultStudy, null);
        }

        #region GetObject tests

        /// <summary>
        /// Test of the GetObject REST API function for all valid test models loaded in sessions on the server side.
        /// This test is performed for the GetObject function without full children serialization.
        /// </summary>
        [TestMethod]
        public void TestGetObject()
        {  
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testGetObject(session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// Tests the GetObject REST API function for a model without child objects full serialization, specifying the IdmStudy 
        /// for properties comparison and the session ID associated to the study.
        /// If the session ID is null, considers to call the REST API without the sessionId parameter (should be used for the 
        /// server default study).
        /// </summary>
        /// <param name="study">The study for properties comparison</param>
        /// <param name="sessionId">The session ID associated to the study (null for the default study)</param>
        private void testGetObject(IdmStudy study, string sessionId)
        {
            MainSystem currentSystem = study.MainSystem;

            TestsHelper.IdmCicHttpResponse response;
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(sessionId))
            {
                parameters.Add("sessionId", sessionId);
            }

            // Test the main system with some properties
            response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", true, parameters);

            dynamic dynamicMainSystem = JsonConvert.DeserializeObject<dynamic>(response.StringValue);
            Assert.AreEqual(currentSystem.ProjectName, (string)dynamicMainSystem.ProjectName);
            Assert.AreEqual(currentSystem.Comment, (string)dynamicMainSystem.Comment);
            Assert.AreEqual(currentSystem.PropellantMassMargin, (double)dynamicMainSystem.PropellantMassMargin);
            Assert.AreEqual(currentSystem.Elements.Count, (int)dynamicMainSystem.Elements.Count);

            parameters.Add("fullId", "");

            // Test elements with retrieved references
            foreach (dynamic dynamicElementRef in dynamicMainSystem.Elements)
            {
                // Ensure it's a reference retrieving the full ID (not null) and that a mandatory element
                // property is not defined in this dynamic object.
                Assert.IsNotNull(dynamicElementRef.FullId);
                Assert.IsNull(dynamicElementRef.MassTarget);

                // Check this element ref
                string elementFullId = dynamicElementRef.FullId;
                IdmObject elObject = study.GetObjectByFullId(elementFullId);

                Assert.IsInstanceOfType(elObject, typeof(Element));

                Element element = elObject as Element;

                // Retrieve the element data through API
                parameters["fullId"] = elementFullId;

                response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", true, parameters);

                dynamic dynamicElement = JsonConvert.DeserializeObject<dynamic>(response.StringValue);
                Assert.AreEqual(element.Name, (string)dynamicElement.Name);
                Assert.AreEqual(element.MassTarget, (double)dynamicElement.MassTarget);
                Assert.AreEqual(element.MassMargin, (double)dynamicElement.MassMargin);
                Assert.AreEqual(element.RelatedSubsystems.Count, (int)dynamicElement.RelatedSubsystems.Count);
            }

            // Try to get a non existing object: should raise an exception
            parameters["fullId"] = "elis.2536";
            response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", false, parameters);
        }

        /// <summary>
        /// Test of the GetObject REST API function for all valid test models loaded in sessions on the server side.
        /// This test is performed for the GetObject function with full children serialization.
        /// </summary>
        [TestMethod]
        public void TestGetObjectFullSerialization()
        {
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testGetObjectFullSerialization(session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// Tests the GetObject REST API function for a model with child objects full serialization, specifying the IdmStudy 
        /// for properties comparison and the session ID associated to the study.
        /// If the session ID is null, considers to call the REST API without the sessionId parameter (should be used for the 
        /// server default study).
        /// </summary>
        /// <param name="study">The study for properties comparison</param>
        /// <param name="sessionId">The session ID associated to the study (null for the default study)</param>
        private void testGetObjectFullSerialization(IdmStudy study, string sessionId)
        {
            MainSystem currentSystem = study.MainSystem;

            TestsHelper.IdmCicHttpResponse response;
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(sessionId))
            {
                parameters.Add("sessionId", sessionId);
            }

            parameters.Add("getChildsAsReference", false.ToString());

            // Test the main system with some properties
            response = TestsHelper.GetServerResponseWithStatusCheck("GetObject", true, parameters);

            dynamic dynamicMainSystem = JsonConvert.DeserializeObject<dynamic>(response.StringValue);
            Assert.AreEqual(currentSystem.Elements.Count, (int)dynamicMainSystem.Elements.Count);

            // Test elements with retrieved references
            foreach (dynamic dynamicElement in dynamicMainSystem.Elements)
            {
                // Ensure the dynamic object has all properties (fully serialized)
                Assert.IsNotNull(dynamicElement.FullId);
                Assert.IsNotNull(dynamicElement.Id);
                Assert.IsNotNull(dynamicElement.MassTarget);
                Assert.IsNotNull(dynamicElement.MassMargin);
                Assert.IsNotNull(dynamicElement.RelatedSubsystems);

                // Get the element from the current system
                Element element = currentSystem.GetElement((string)dynamicElement.Id);

                // Check same properties
                Assert.AreEqual(element.Name, (string)dynamicElement.Name);
                Assert.AreEqual(element.MassTarget, (double)dynamicElement.MassTarget);
                Assert.AreEqual(element.MassMargin, (double)dynamicElement.MassMargin);
                Assert.AreEqual(element.RelatedSubsystems.Count, (int)dynamicElement.RelatedSubsystems.Count);

                // Test on the element sub objects (related subsystems)
                foreach (dynamic dynamicRelatedSubsystem in dynamicElement.RelatedSubsystems)
                {
                    // Ensure the dynamic object has all properties (fully serialized)
                    Assert.IsNotNull(dynamicRelatedSubsystem.FullId);
                    Assert.IsNotNull(dynamicRelatedSubsystem.Id);

                    // Get the related subsystem from the current system
                    RelatedSubsystem relatedSubsystem = element.GetRelatedSubsystem((string)dynamicRelatedSubsystem.Id);
                    Assert.AreEqual(relatedSubsystem.SortOrder, (int)dynamicRelatedSubsystem.SortOrder);
                }
            }
        }

        #endregion

        #region MCI tests

        /// <summary>
        /// Test the GetMass REST API function for all valid test models loaded in sessions on the server side.
        /// Test is performed for all configurations and every possible calculation options.
        /// </summary>
        [TestMethod]
        public void TestGetMass()
        {
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testMci(MciTest.Mass, session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// Test the GetCog REST API function for all valid test models loaded in sessions on the server side.
        /// Test is performed for all configurations and every possible calculation options.
        /// </summary>
        [TestMethod]
        public void TestGetCog()
        {
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testMci(MciTest.Cog, session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// Test the GetInertiaMatrix REST API function for all valid test models loaded in sessions on the server side.
        /// Test is performed for all configurations and every possible calculation options.
        /// </summary>
        [TestMethod]
        public void TestGetInertiaMatrix()
        {
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testMci(MciTest.InertiaMatrix, session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// This enum can be used by tests functions to determine which type of MCI test should be performed.
        /// </summary>
        private enum MciTest
        {
            /// <summary>
            /// Test the system mass
            /// </summary>
            Mass,
            /// <summary>
            /// Test the system center of gravity
            /// </summary>
            Cog,
            /// <summary>
            /// Test the system inertia matrix
            /// </summary>
            InertiaMatrix
        }

        /// <summary>
        /// Perform a specific MCI test specifying the IdmStudy for results comparison and the session ID associated to the study.
        /// If the session ID is null, considers to call the REST API without the sessionId parameter (should be used for the 
        /// server default study).
        /// </summary>
        /// <param name="testToPerform">The type of test to perform</param>
        /// <param name="study">The study for results comparison</param>
        /// <param name="sessionId">The session ID associated to the study (null for the default study)</param>
        private void testMci(MciTest testToPerform, IdmStudy study, string sessionId)
        {
            string functionToCall = "Get" + testToPerform.ToString();
            
            MainSystem currentSystem = study.MainSystem;

            TestsHelper.IdmCicHttpResponse response;

            // Test the default configuration
            foreach (KeyValuePair<MainSystemMassOptions, Dictionary<string, string>> kvp in getAllMciParameters(sessionId))
            {
                MainSystemMassOptions massOptions = kvp.Key;
                Dictionary<string, string> parameters = kvp.Value;

                response = TestsHelper.GetServerResponseWithStatusCheck(functionToCall, true, parameters);

                switch (testToPerform)
                {
                    case MciTest.Mass:
                        double expectedMass = currentSystem.GetMass(massOptions);
                        double retrievedMass = JsonConvert.DeserializeObject<double>(response.StringValue);
                        Assert.AreEqual(expectedMass, retrievedMass);
                        break;
                    case MciTest.Cog:
                        double[] expectedCog = currentSystem.GetCog(massOptions).ToArray();
                        double[] retrievedCog = JsonConvert.DeserializeObject<double[]>(response.StringValue);
                        CollectionAssert.AreEqual(expectedCog, retrievedCog);
                        break;
                    case MciTest.InertiaMatrix:
                        double[,] expectedInertia = currentSystem.GetInertiaMatrix(massOptions).ToArray();
                        double[,] retrievedInertia = JsonConvert.DeserializeObject<double[,]>(response.StringValue);
                        CollectionAssert.AreEqual(expectedInertia, retrievedInertia);
                        break;
                }
            }

            // Test all configurations
            foreach (Configuration configuration in currentSystem.Configurations)
            {
                foreach (KeyValuePair<MainSystemMassOptions, Dictionary<string, string>> kvp in getAllMciParameters(sessionId))
                {
                    MainSystemMassOptions massOptions = kvp.Key;
                    Dictionary<string, string> parameters = kvp.Value;

                    parameters["configurationId"] = configuration.Id;

                    response = TestsHelper.GetServerResponseWithStatusCheck(functionToCall, true, parameters);

                    switch (testToPerform)
                    {
                        case MciTest.Mass:
                            double expectedMass = currentSystem.GetMass(massOptions, configuration);
                            double retrievedMass = JsonConvert.DeserializeObject<double>(response.StringValue);
                            Assert.AreEqual(expectedMass, retrievedMass);
                            break;
                        case MciTest.Cog:
                            double[] expectedCog = currentSystem.GetCog(massOptions, configuration).ToArray();
                            double[] retrievedCog = JsonConvert.DeserializeObject<double[]>(response.StringValue);
                            CollectionAssert.AreEqual(expectedCog, retrievedCog);
                            break;
                        case MciTest.InertiaMatrix:
                            double[,] expectedInertia = currentSystem.GetInertiaMatrix(massOptions, configuration).ToArray();
                            double[,] retrievedInertia = JsonConvert.DeserializeObject<double[,]>(response.StringValue);
                            CollectionAssert.AreEqual(expectedInertia, retrievedInertia);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Get needed parameters for a MCI test with all possibilities.
        /// Each returned item is a KeyValuePair in which:
        /// - The key is the IDM-CIC mass options to use for calling the needed function by C#.
        /// - The value is a dictionary that defines calculation options to pass to the REST API function.
        /// </summary>
        /// <param name="sessionId">The session ID to add to the parameters (if null, the parameter won't be specified)</param>
        /// <returns>Enumeration of mass options to consider and parameters to send to the REST API</returns>
        private IEnumerable<KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>> getAllMciParameters(string sessionId)
        {
            MainSystemMassOptions defaultMassOptions = MainSystemMassOptions.IncludeElementActivation | MainSystemMassOptions.IncludeTankContent;

            Dictionary<string, string> parameters = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(sessionId))
            {
                parameters.Add("sessionId", sessionId);
            }

            // No margin
            parameters["IEqMar"] = false.ToString();
            parameters["IElMar"] = false.ToString();
            parameters["ISysProMar"] = false.ToString();
            MainSystemMassOptions currentMassOptions = defaultMassOptions;
            yield return new KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>(currentMassOptions, parameters);

            // Equipments mass margin
            parameters["IEqMar"] = true.ToString();
            parameters["IElMar"] = false.ToString();
            parameters["ISysProMar"] = false.ToString();
            currentMassOptions = defaultMassOptions | MainSystemMassOptions.IncludeEquipmentMargin;
            yield return new KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>(currentMassOptions, parameters);

            // Elements mass margin
            parameters["IEqMar"] = false.ToString();
            parameters["IElMar"] = true.ToString();
            parameters["ISysProMar"] = false.ToString();
            currentMassOptions = defaultMassOptions | MainSystemMassOptions.IncludeElementMargin;
            yield return new KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>(currentMassOptions, parameters);

            // System propellant mass margin
            parameters["IEqMar"] = false.ToString();
            parameters["IElMar"] = false.ToString();
            parameters["ISysProMar"] = true.ToString();
            currentMassOptions = defaultMassOptions | MainSystemMassOptions.IncludeSystemPropellantMargin;
            yield return new KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>(currentMassOptions, parameters);

            // Equipments and elements mass margins
            parameters["IEqMar"] = true.ToString();
            parameters["IElMar"] = true.ToString();
            parameters["ISysProMar"] = false.ToString();
            currentMassOptions = defaultMassOptions | MainSystemMassOptions.IncludeEquipmentMargin | MainSystemMassOptions.IncludeElementMargin;
            yield return new KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>(currentMassOptions, parameters);

            // Equipments and system propellant mass margins
            parameters["IEqMar"] = true.ToString();
            parameters["IElMar"] = false.ToString();
            parameters["ISysProMar"] = true.ToString();
            currentMassOptions = defaultMassOptions | MainSystemMassOptions.IncludeEquipmentMargin | MainSystemMassOptions.IncludeSystemPropellantMargin;
            yield return new KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>(currentMassOptions, parameters);

            // Elements and system propellant mass margins
            parameters["IEqMar"] = false.ToString();
            parameters["IElMar"] = true.ToString();
            parameters["ISysProMar"] = true.ToString();
            currentMassOptions = defaultMassOptions | MainSystemMassOptions.IncludeElementMargin | MainSystemMassOptions.IncludeSystemPropellantMargin;
            yield return new KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>(currentMassOptions, parameters);

            // All margins
            parameters["IEqMar"] = true.ToString();
            parameters["IElMar"] = true.ToString();
            parameters["ISysProMar"] = true.ToString();
            currentMassOptions = defaultMassOptions | MainSystemMassOptions.IncludeEquipmentMargin | MainSystemMassOptions.IncludeElementMargin
                | MainSystemMassOptions.IncludeSystemPropellantMargin;
            yield return new KeyValuePair<MainSystemMassOptions, Dictionary<string, string>>(currentMassOptions, parameters);
        }

        #endregion

        #region Power tests

        /// <summary>
        /// Test the GetConsumedPower REST API function for all valid test models loaded in sessions on the server side.
        /// Test is performed for all system modes, all configurations and every possible calculation options.
        /// </summary>
        [TestMethod]
        public void TestGetConsumedPower()
        {
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testPower(PowerTest.ConsumedPower, session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// Test the GetDissipatedPower REST API function for all valid test models loaded in sessions on the server side.
        /// Test is performed for all system modes, all configurations and every possible calculation options.
        /// </summary>
        [TestMethod]
        public void TestGetDissipatedPower()
        {
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testPower(PowerTest.DissipatedPower, session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// Test the GetDutyCycleConsumedPower REST API function for all valid test models loaded in sessions on the server side.
        /// Test is performed for all configurations and every possible calculation options.
        /// </summary>
        [TestMethod]
        public void TestGetDutyCycleConsumedPower()
        {
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testPower(PowerTest.DutyCycleConsumedPower, session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// Test the GetDutyCycleDissipatedPower REST API function for all valid test models loaded in sessions on the server side.
        /// Test is performed for all configurations and every possible calculation options.
        /// </summary>
        [TestMethod]
        public void TestGetDutyCycleDissipatedPower()
        {
            foreach (ModelSessionData session in TestsHelper.LoadAllTestModels())
            {
                testPower(PowerTest.DutyCycleDissipatedPower, session.IdmStudy, session.SessionId);
            }
        }

        /// <summary>
        /// This enum can be used by tests functions to determine which type of MCI test should be performed.
        /// </summary>
        private enum PowerTest
        {
            /// <summary>
            /// The system consumed power
            /// </summary>
            ConsumedPower,
            /// <summary>
            /// The system dissipated power
            /// </summary>
            DissipatedPower,
            /// <summary>
            /// The system duty cycle consumed power
            /// </summary>
            DutyCycleConsumedPower,
            /// <summary>
            /// The system duty cycle dissipated power
            /// </summary>
            DutyCycleDissipatedPower
        }
        
        /// <summary>
        /// Perform a specific power test specifying the IdmStudy for results comparison and the session ID associated to the study.
        /// If the session ID is null, considers to call the REST API without the sessionId parameter (should be used for the 
        /// server default study).
        /// </summary>
        /// <param name="testToPerform">The type of test to perform</param>
        /// <param name="study">The study for results comparison</param>
        /// <param name="sessionId">The session ID associated to the study (null for the default study)</param>
        private void testPower(PowerTest testToPerform, IdmStudy study, string sessionId)
        {
            string functionToCall = "Get" + testToPerform.ToString();

                MainSystem currentSystem = study.MainSystem;

                TestsHelper.IdmCicHttpResponse response;

                // Test the default configuration
                foreach (KeyValuePair<MainSystemPowerOptions, Dictionary<string, string>> kvp in getAllPowerParameters(sessionId))
                {
                    MainSystemPowerOptions powerOptions = kvp.Key;
                    Dictionary<string, string> parameters = kvp.Value;

                    if (testToPerform == PowerTest.ConsumedPower || testToPerform == PowerTest.DissipatedPower)
                    {
                        // By system mode !
                        foreach (SystemMode systemMode in currentSystem.SystemModes)
                        {
                            parameters["systemModeId"] = systemMode.Id;

                            response = TestsHelper.GetServerResponseWithStatusCheck(functionToCall, true, parameters);

                            double retrievedPower = JsonConvert.DeserializeObject<double>(response.StringValue);

                            double expectedPower = 0;

                            if (testToPerform == PowerTest.ConsumedPower)
                            {
                                // Consumed
                                expectedPower = currentSystem.GetPower(systemMode, powerOptions);
                            }
                            else
                            {
                                // Dissipated
                                expectedPower = currentSystem.GetDissipation(systemMode, powerOptions);
                            }

                            Assert.AreEqual(expectedPower, retrievedPower);
                        }
                    }
                    else
                    {
                        // Duty cycle
                        response = TestsHelper.GetServerResponseWithStatusCheck(functionToCall, true, parameters);

                        double retrievedPower = JsonConvert.DeserializeObject<double>(response.StringValue);

                        double expectedPower = 0;

                        if (testToPerform == PowerTest.ConsumedPower)
                        {
                            // Consumed
                            expectedPower = currentSystem.GetDutyCyclePower(powerOptions);
                        }
                        else
                        {
                            // Dissipated
                            expectedPower = currentSystem.GetDutyCycleDissipation(powerOptions);
                        }

                        Assert.AreEqual(expectedPower, retrievedPower);
                    }
                }

            // Test all configurations
            foreach (Configuration configuration in currentSystem.Configurations)
            {
                foreach (KeyValuePair<MainSystemPowerOptions, Dictionary<string, string>> kvp in getAllPowerParameters(sessionId))
                {
                    MainSystemPowerOptions powerOptions = kvp.Key;
                    Dictionary<string, string> parameters = kvp.Value;

                    parameters["configurationId"] = configuration.Id;

                    if (testToPerform == PowerTest.ConsumedPower || testToPerform == PowerTest.DissipatedPower)
                    {
                        // By system mode !
                        foreach (SystemMode systemMode in currentSystem.SystemModes)
                        {
                            parameters["systemModeId"] = systemMode.Id;

                            response = TestsHelper.GetServerResponseWithStatusCheck(functionToCall, true, parameters);

                            double retrievedPower = JsonConvert.DeserializeObject<double>(response.StringValue);

                            double expectedPower = 0;

                            if (testToPerform == PowerTest.ConsumedPower)
                            {
                                // Consumed
                                expectedPower = currentSystem.GetPower(systemMode, powerOptions, configuration);
                            }
                            else
                            {
                                // Dissipated
                                expectedPower = currentSystem.GetDissipation(systemMode, powerOptions, configuration);
                            }

                            Assert.AreEqual(expectedPower, retrievedPower);
                        }
                    }
                    else
                    {
                        // Duty cycle
                        response = TestsHelper.GetServerResponseWithStatusCheck(functionToCall, true, parameters);

                        double retrievedPower = JsonConvert.DeserializeObject<double>(response.StringValue);

                        double expectedPower = 0;

                        if (testToPerform == PowerTest.ConsumedPower)
                        {
                            // Consumed
                            expectedPower = currentSystem.GetDutyCyclePower(powerOptions, configuration);
                        }
                        else
                        {
                            // Dissipated
                            expectedPower = currentSystem.GetDutyCycleDissipation(powerOptions, configuration);
                        }

                        Assert.AreEqual(expectedPower, retrievedPower);
                    }
                }
            }
        }

        /// <summary>
        /// Get needed parameters for a power test with all possibilities.
        /// Each returned item is a KeyValuePair in which:
        /// - The key is the IDM-CIC power options to use for calling the needed function by C#.
        /// - The value is a dictionary that defines calculation options to pass to the REST API function.
        /// </summary>
        /// <param name="sessionId">The session ID to add to the parameters (if null, the parameter won't be specified)</param>
        /// <returns>Enumeration of power options to consider and parameters to send to the REST API</returns>
        private IEnumerable<KeyValuePair<MainSystemPowerOptions, Dictionary<string, string>>> getAllPowerParameters(string sessionId)
        {
            MainSystemPowerOptions defaultPowerOptions = MainSystemPowerOptions.IncludeElementActivation;

            Dictionary<string, string> parameters = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(sessionId))
            {
                parameters.Add("sessionId", sessionId);
            }

            // No margin
            parameters["IEqMar"] = false.ToString();
            parameters["IElMar"] = false.ToString();
            MainSystemPowerOptions currentPowerOptions = defaultPowerOptions;
            yield return new KeyValuePair<MainSystemPowerOptions, Dictionary<string, string>>(currentPowerOptions, parameters);

            // Equipments power margin
            parameters["IEqMar"] = true.ToString();
            parameters["IElMar"] = false.ToString();
            currentPowerOptions = defaultPowerOptions | MainSystemPowerOptions.IncludeEquipmentMargin;
            yield return new KeyValuePair<MainSystemPowerOptions, Dictionary<string, string>>(currentPowerOptions, parameters);

            // Elements power margin
            parameters["IEqMar"] = false.ToString();
            parameters["IElMar"] = true.ToString();
            currentPowerOptions = defaultPowerOptions | MainSystemPowerOptions.IncludeElementMargin;
            yield return new KeyValuePair<MainSystemPowerOptions, Dictionary<string, string>>(currentPowerOptions, parameters);

            // All margins
            parameters["IEqMar"] = true.ToString();
            parameters["IElMar"] = true.ToString();
            currentPowerOptions = defaultPowerOptions | MainSystemPowerOptions.IncludeEquipmentMargin | MainSystemPowerOptions.IncludeElementMargin;
            yield return new KeyValuePair<MainSystemPowerOptions, Dictionary<string, string>>(currentPowerOptions, parameters);
        }

        #endregion
    }
}
