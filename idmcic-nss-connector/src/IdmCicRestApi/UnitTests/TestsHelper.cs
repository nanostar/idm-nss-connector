﻿// License header goes here.
using IdmCicRestApi.Server;
using IdmCicRestApi.Server.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace UnitTests
{
    /// <summary>
    /// Some helpers functions for unit tests
    /// </summary>
    public static class TestsHelper
    {
        /// <summary>
        /// This structure helps to get a server reponse for an HTTP request made by a call to GetServerResponse or 
        /// GetServerResponseWithStatusCheck functions.
        /// </summary>
        public struct IdmCicHttpResponse
        {
            /// <summary>
            /// The retrieved HTTP response message.
            /// </summary>
            public HttpResponseMessage ResponseMessage;
            /// <summary>
            /// The response as string.
            /// </summary>
            public string StringValue;
            /// <summary>
            /// The response as byte array.
            /// </summary>
            public byte[] ByteValue;
        }

        /// <summary>
        /// An HTTP client to execute some queries.
        /// </summary>
        private static readonly HttpClient client = new HttpClient();

        /// <summary>
        /// This constant is used by test methods and can be changed if the port is not available on the tester computer
        /// </summary>
        public const int DEFAULT_SERVER_PORT = 80;


        /// <summary>
        /// Starts the IDM-CIC HTTP server with the default port.
        /// </summary>
        public static void StartServerWithDefaultPort()
        {
            IdmCicHttpServer.Instance.Port = DEFAULT_SERVER_PORT;
            IdmCicHttpServer.Instance.Start();
        }

        /// <summary>
        /// Retrieve a model path for performing a unit test within provided test models.
        /// The specified model name should be a file name provided in the "TestModels" folder.
        /// It can be specified without extension (if so, considers automatically the ".idm" extension).
        /// </summary>
        /// <param name="modelName">The model file name (can be specified without the extension)</param>
        /// <returns>The full model file path</returns>
        /// <exception cref="FileNotFoundException">Thrown if the specified model file is not found within the "TestModels" folder</exception>
        public static string GetTestModelPath(string modelName)
        {
            string assemblyLocation = Assembly.GetExecutingAssembly().Location;
            assemblyLocation = assemblyLocation.Replace(Path.GetFileName(assemblyLocation), "");

            string testModelsPath = Path.Combine(assemblyLocation, "TestModels");

            string modelFileName = !string.IsNullOrEmpty(Path.GetExtension(modelName)) ? modelName : modelName + ".idm";

            string filePath = Path.Combine(testModelsPath, modelFileName);

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("The test model \"" + modelName + "\" was not found");
            }

            return filePath;
        }

        /// <summary>
        /// Loads all valid test models in the server session and returns the assicoated ModelSessionData object.
        /// </summary>
        /// <returns>Enumeration of ModelSessionData objects for loaded models</returns>
        public static IEnumerable<ModelSessionData> LoadAllTestModels()
        {
            List<string> filesToTest = new List<string>()
            {
                GetTestModelPath("CIC-SAT"),
                GetTestModelPath("Orbit-CIC-SAT")
            };

            foreach (string fileToLoad in filesToTest)
            {
                yield return IdmCicHttpServer.Instance.LoadIdmModel(fileToLoad);
            }
        }

        #region Assert extensions

        /// <summary>
        /// Ensure an action throw an exception of a specific type if needed and if so, if the exception should be
        /// exactely of this type or any derived type.
        /// </summary>
        /// <param name="action">Action to execute</param>
        /// <param name="type">Optionnal exception type to check</param>
        /// <param name="exactType">If true, use strict type comparison, else use IsAssignableFrom function</param>
        public static void AssertThrowException(Action action, Type type = null, bool exactType = false)
        {
            try
            {
                action.Invoke();
                Assert.Fail("The action didn't throw any exception");
            }
            catch (Exception e)
            {
                if (type != null)
                {
                    if (exactType && e.GetType() != type)
                    {
                        Assert.Fail("The action didn't throw an exception of type " + type.FullName);
                    }
                    else if (!exactType && type.IsAssignableFrom(e.GetType()))
                    {
                        Assert.Fail("The action didn't throw an exception assignable from type " + type.FullName);
                    }
                }
            }
        }

        /// <summary>
        /// Assert two objects are equals converting the expected object into a JSON object and parsing the string value specified.
        /// Use the DeepEquals function of JToken.
        /// </summary>
        /// <param name="expected">Expected object that will be converted to a JSON object</param>
        /// <param name="value">String value that will me parsed to a JSON object</param>
        public static void AssertJsonObjectsEqual(object expected, string value)
        {
            JObject expectedJson = JObject.FromObject(expected);
            JObject valueJson = JObject.Parse(value);
            
            Assert.IsTrue(JToken.DeepEquals(expectedJson, valueJson));
        }

        /// <summary>
        /// Assert two arrays are equals converting the expected object into a JSON array and parsing the string value specified.
        /// Use the DeepEquals function of JToken.
        /// </summary>
        /// <param name="expected">Expected object that will be converted to a JSON array (should be an array of any dimension)</param>
        /// <param name="value">String value that will me parsed to a JSON array</param>
        public static void AssertJsonArraysEqual(object expected, string value)
        {
            JArray expectedJson = new JArray(expected);
            JArray valueJson = JArray.Parse(value);

            Assert.IsTrue(JToken.DeepEquals(expectedJson, valueJson));
        }

        /// <summary>
        /// Assert two values are equals converting the expected object into a JSON value and parsing the string value specified.
        /// Use the DeepEquals function of JToken.
        /// </summary>
        /// <param name="expected">Expected object that will be converted to a JSON value (should be a simple type)</param>
        /// <param name="value">String value that will me parsed to a JSON token</param>
        public static void AssertJsonValuesEqual(object expected, string value)
        {
            JValue expectedJson = new JValue(expected);
            JToken valueJson = JToken.Parse(value);

            Assert.IsTrue(JToken.DeepEquals(expectedJson, valueJson));
        }

        #endregion

        #region REST API calls

        /// <summary>
        /// Call the IDM-CIC HTTP server using an action name, optional parameters and check if the action is successful or not considering
        /// the "isSuccess" parameter, performing an Assert operation on the server response status.
        /// </summary>
        /// <param name="action">The action to call</param>
        /// <param name="isSuccess">Defines if the request should be successful or not</param>
        /// <param name="parameters">Optional parameters to add</param>
        /// <returns>An IdmCicHttpResponse object</returns>
        public static IdmCicHttpResponse GetServerResponseWithStatusCheck(string action, bool isSuccess, IEnumerable<KeyValuePair<string, string>> parameters = null)
        {
            IdmCicHttpResponse response = GetServerResponse(action, parameters);

            Assert.AreEqual(isSuccess, response.ResponseMessage.IsSuccessStatusCode);

            return response;
        }

        /// <summary>
        /// Call the IDM-CIC HTTP server using an action name and optional parameters.
        /// </summary>
        /// <param name="action">The action to call</param>
        /// <param name="parameters">Optional parameters to add</param>
        /// <returns>An IdmCicHttpResponse object</returns>
        public static IdmCicHttpResponse GetServerResponse(string action, IEnumerable<KeyValuePair<string, string>> parameters = null)
        {
            if (parameters == null)
            {
                parameters = new Dictionary<string, string>();
            }
            
            string url = "http://localhost:" + IdmCicHttpServer.Instance.Port.ToString() + "/IdmCicWebService/" + action;

            string parametersAsString = string.Join("&", parameters.Select(kvp => string.Format("{0}={1}", kvp.Key, kvp.Value)));

            return GetResponse(url, parametersAsString).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Retrieve asynchronously an HTTP response message when calling an URL and returns an IdmCicHttpResponse object based on the response.
        /// Use the GET protocol to do so (parameters should be a string URL encoded to add to the URL) or null for no parameters.
        /// </summary>
        /// <param name="url">The URL to be called by the HTTP client</param>
        /// <param name="parameters">Get parameters as string</param>
        /// <returns>an IdmCicHttpResponse object</returns>
        private static async Task<IdmCicHttpResponse> GetResponse(string url, string parameters)
        {
            if (!string.IsNullOrEmpty(parameters))
            {
                // Add parameters to the URL
                url += "?" + parameters;
            }

            // Get the HTTP response for the URL call
            HttpResponseMessage response = await client.GetAsync(url);

            // Create the IdmCicHttpResponse object and return it
            IdmCicHttpResponse responseReturned;
            responseReturned.ResponseMessage = response;
            responseReturned.StringValue = await response.Content.ReadAsStringAsync();
            responseReturned.ByteValue = await response.Content.ReadAsByteArrayAsync();

            return responseReturned;
        }

        #endregion
    }
}
