﻿// License header goes here.
using IdmCicRestApi.Helpers;
using NLog;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace IdmCicRestApi.Server
{
    /// <summary>
    /// Message handler of the HTTP server to log incoming HTTP requests.
    /// </summary>
    internal class ServerMessageHandler : DelegatingHandler
    {
        /// <summary>
        /// Overrides the SendAsync method to log first the incoming request.
        /// The log message contains the method (GET / POST), the requested URI and the client IP. 
        /// </summary>
        /// <param name="request">The HTTP request message to send to the server.</param>
        /// <param name="cancellationToken">The HTTP request message to send to the server.</param>
        /// <returns>Returns System.Threading.Tasks.Task`1. The task object representing the asynchronous operation.</returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // Log incoming request
            string message = ServiceHelper.getLogMessageFromRequest(request);
            LogManager.GetLogger("ServiceRequests").Info(message);

            // Return base treatment
            return base.SendAsync(request, cancellationToken);
        }

        
    }
}
