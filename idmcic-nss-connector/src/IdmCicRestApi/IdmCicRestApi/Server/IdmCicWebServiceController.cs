﻿// License header goes here.
using IdmCic.API.Model;
using IdmCic.API.Model.Configurations;
using IdmCic.API.Model.Power;
using IdmCic.API.Utils.Calculation.MCI;
using IdmCic.API.Utils.Calculation.Power;
using IdmCicRestApi.Helpers;
using IdmCicRestApi.Server.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;

namespace IdmCicRestApi.Server
{
    /// <summary>
    /// This class is an API controller that is used by the IDM-CIC HTTP server.
    /// It exposes functions that can be called through HTTP requests and use the HTTP server singleton to get data from loaded IDM-CIC models
    /// </summary>
    public class IdmCicWebServiceController : ApiController
    {
        /// <summary>
        /// Returns all IDM files exposed by the server as a JSON two dimensional array of string.
        /// </summary>
        /// <returns>JSON {string, string}[]</returns>
        [HttpGet]
        public IHttpActionResult IdmFilesList()
        {
            IEnumerable<string> entries = IdmCicHttpServer.Instance.ExposedIdmFiles.Select(
                d => string.Format("\"{0}\": [{1}]", d.FilePath, string.Join(",", d.CustomName)));

            return Json(IdmCicHttpServer.Instance.ExposedIdmFiles.ToDictionary(), ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Returns a flag as boolean indicating if the server has a default model loaded.
        /// </summary>
        /// <returns>JSON boolean</returns>
        [HttpGet]
        public IHttpActionResult HasDefaultModel()
        {
            return Json(IdmCicHttpServer.Instance.DefaultStudy != null, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Loads an IDM-CIC model from a model path in the server computer and returns the new session ID as string.
        /// </summary>
        /// <param name="modelPath">The path of the IDM file to open on the server</param>
        /// <returns>JSON string</returns>
        [HttpGet]
        public IHttpActionResult LoadIdmModel(string modelPath)
        {
            ModelSessionData modelSession = IdmCicHttpServer.Instance.LoadIdmModel(modelPath);

            return Json(modelSession.SessionId, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Loads an IDM object by its full ID within the IDM-CIC model associated to the specified session ID 
        /// and returns the IDM object serialized in JSON.
        /// If the specified full ID is null or empty, returns the main system.
        /// If the session ID is null or empty, uses the default IDM-CIC study of the server if it's not null.
        /// </summary>
        /// <param name="sessionId">The model session ID (default = null)</param>
        /// <param name="fullId">The IDM object full ID (default = null)</param>
        /// <param name="getChildsAsReference">If true, retrieve all child objects as reference, else retrieve all 
        /// child objects as fully serialized objects (default = true)</param>
        /// <returns>JSON dictionary of string / object</returns>
        [HttpGet]
        public IHttpActionResult GetObject(string sessionId = null, string fullId = null, bool getChildsAsReference = true)
        {
            IdmStudy study = IdmCicHttpServer.Instance.GetIdmStudy(sessionId);

            IdmObject idmObject = string.IsNullOrEmpty(fullId) ? study.MainSystem : study.GetObjectByFullId(fullId);

            Dictionary<string, object> serializedObject = ServiceHelper.getIdmObjectAsDictionary(idmObject, getChildsAsReference);

            return Json(serializedObject, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Returns the main system mass the IDM-CIC model associated to the specified session ID 
        /// with specific calculation options and for an IDM-CIC configuration.
        /// It always considers elements activation and tank contents.
        /// If the configuration ID is not specified, it will use the IDM-CIC reference configuration.
        /// If the session ID is null or empty, uses the default IDM-CIC study of the server if it's not null.
        /// </summary>
        /// <param name="sessionId">The model session ID (default = null)</param>
        /// <param name="iEqMar">Include or not equipments mass margin (default = false)</param>
        /// <param name="iElMar">Include or not elements mass margin (default = false)</param>
        /// <param name="iSysProMar">Include or not system propellant mass margin (default = false)</param>
        /// <param name="configurationId">The configuration ID (default = null)</param>
        /// <returns>JSON double</returns>
        [HttpGet]
        public IHttpActionResult GetMass(string sessionId = null, bool iEqMar = false, bool iElMar = false, 
            bool iSysProMar = false, string configurationId = null)
        {
            IdmStudy study = IdmCicHttpServer.Instance.GetIdmStudy(sessionId);

            Configuration configuration = ServiceHelper.getConfiguration(study, configurationId);
            MainSystemMassOptions options = ServiceHelper.getMassOptions(iEqMar, iElMar, iSysProMar);

            double mass = study.MainSystem.GetMass(options, configuration);

            return Json(mass, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Returns the main system center of gravity the IDM-CIC model associated to the specified session ID 
        /// with specific calculation options and for an IDM-CIC configuration.
        /// It always considers elements activation and tank contents.
        /// If the configuration ID is not specified, it will use the IDM-CIC reference configuration.
        /// If the session ID is null or empty, uses the default IDM-CIC study of the server if it's not null.
        /// </summary>
        /// <param name="sessionId">The model session ID (default = null)</param>
        /// <param name="iEqMar">Include or not equipments mass margin (default = false)</param>
        /// <param name="iElMar">Include or not elements mass margin (default = false)</param>
        /// <param name="iSysProMar">Include or not system propellant mass margin (default = false)</param>
        /// <param name="configurationId">The configuration ID (default = null)</param>
        /// <returns>JSON double[3]</returns>
        [HttpGet]
        public IHttpActionResult GetCog(string sessionId = null, bool iEqMar = false, bool iElMar = false, 
            bool iSysProMar = false, string configurationId = null)
        {
            IdmStudy study = IdmCicHttpServer.Instance.GetIdmStudy(sessionId);

            Configuration configuration = ServiceHelper.getConfiguration(study, configurationId);
            MainSystemMassOptions options = ServiceHelper.getMassOptions(iEqMar, iElMar, iSysProMar);

            double[] cog = study.MainSystem.GetCog(options, configuration).ToArray();
            
            return Json(cog, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Returns the main system inertia matrix the IDM-CIC model associated to the specified session ID 
        /// with specific calculation options and for an IDM-CIC configuration.
        /// It always considers elements activation and tank contents.
        /// If the configuration ID is not specified, it will use the IDM-CIC reference configuration.
        /// If the session ID is null or empty, uses the default IDM-CIC study of the server if it's not null.
        /// </summary>
        /// <param name="sessionId">The model session ID (default = null)</param>
        /// <param name="iEqMar">Include or not equipments mass margin (default = false)</param>
        /// <param name="iElMar">Include or not elements mass margin (default = false)</param>
        /// <param name="iSysProMar">Include or not system propellant mass margin (default = false)</param>
        /// <param name="configurationId">The configuration ID (default = null)</param>
        /// <returns>JSON double[3,3]</returns>
        [HttpGet]
        public IHttpActionResult GetInertiaMatrix(string sessionId = null, bool iEqMar = false, bool iElMar = false, 
            bool iSysProMar = false, string configurationId = null)
        {
            IdmStudy study = IdmCicHttpServer.Instance.GetIdmStudy(sessionId);

            Configuration configuration = ServiceHelper.getConfiguration(study, configurationId);
            MainSystemMassOptions options = ServiceHelper.getMassOptions(iEqMar, iElMar, iSysProMar);

            double[,] inertiaMatrix = study.MainSystem.GetInertiaMatrix(options, configuration).ToArray();

            return Json(inertiaMatrix, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Returns the main system consumed power the IDM-CIC model associated to the specified session ID 
        /// for a system mode, with specific calculation options and for an IDM-CIC configuration.
        /// It always considers elements activation.
        /// If the configuration ID is not specified, it will use the IDM-CIC reference configuration.
        /// If the session ID is null or empty, uses the default IDM-CIC study of the server if it's not null.
        /// </summary>
        /// <param name="systemModeId">The system mode ID</param>
        /// <param name="sessionId">The model session ID (default = null)</param>
        /// <param name="iEqMar">Include or not equipments power margin (default = false)</param>
        /// <param name="iElMar">Include or not elements power margin (default = false)</param>
        /// <param name="configurationId">The configuration ID (default = null)</param>
        /// <returns>JSON double</returns>
        [HttpGet]
        public IHttpActionResult GetConsumedPower(string systemModeId, string sessionId = null, bool iEqMar = false, 
            bool iElMar = false, string configurationId = null)
        {
            IdmStudy study = IdmCicHttpServer.Instance.GetIdmStudy(sessionId);

            Configuration configuration = ServiceHelper.getConfiguration(study, configurationId);
            SystemMode systemMode = ServiceHelper.getSystemMode(study, systemModeId);
            MainSystemPowerOptions options = ServiceHelper.getPowerOptions(iEqMar, iElMar);

            double power = study.MainSystem.GetPower(systemMode, options, configuration);

            return Json(power, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Returns the main system dissipated power the IDM-CIC model associated to the specified session ID 
        /// for a system mode, with specific calculation options and for an IDM-CIC configuration.
        /// It always considers elements activation.
        /// If the configuration ID is not specified, it will use the IDM-CIC reference configuration.
        /// If the session ID is null or empty, uses the default IDM-CIC study of the server if it's not null.
        /// </summary>
        /// <param name="systemModeId">The system mode ID</param>
        /// <param name="sessionId">The model session ID (default = null)</param>
        /// <param name="iEqMar">Include or not equipments power margin (default = false)</param>
        /// <param name="iElMar">Include or not elements power margin (default = false)</param>
        /// <param name="configurationId">The configuration ID (default = null)</param>
        /// <returns>JSON double</returns>
        [HttpGet]
        public IHttpActionResult GetDissipatedPower(string systemModeId, string sessionId = null, bool iEqMar = false, 
            bool iElMar = false, string configurationId = null)
        {
            IdmStudy study = IdmCicHttpServer.Instance.GetIdmStudy(sessionId);

            Configuration configuration = ServiceHelper.getConfiguration(study, configurationId);
            SystemMode systemMode = ServiceHelper.getSystemMode(study, systemModeId);
            MainSystemPowerOptions options = ServiceHelper.getPowerOptions(iEqMar, iElMar);

            double dissipation = study.MainSystem.GetDissipation(systemMode, options, configuration);

            return Json(dissipation, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Returns the main system duty cycle consumed power the IDM-CIC model associated to the specified session ID 
        /// with specific calculation options and for an IDM-CIC configuration.
        /// It always considers elements activation.
        /// If the configuration ID is not specified, it will use the IDM-CIC reference configuration.
        /// If the session ID is null or empty, uses the default IDM-CIC study of the server if it's not null.
        /// </summary>
        /// <param name="sessionId">The model session ID (default = null)</param>
        /// <param name="iEqMar">Include or not equipments power margin (default = false)</param>
        /// <param name="iElMar">Include or not elements power margin (default = false)</param>
        /// <param name="configurationId">The configuration ID (default = null)</param>
        /// <returns>JSON double</returns>
        [HttpGet]
        public IHttpActionResult GetDutyCycleConsumedPower(string sessionId = null, bool iEqMar = false, bool iElMar = false, 
            string configurationId = null)
        {
            IdmStudy study = IdmCicHttpServer.Instance.GetIdmStudy(sessionId);

            Configuration configuration = ServiceHelper.getConfiguration(study, configurationId);
            MainSystemPowerOptions options = ServiceHelper.getPowerOptions(iEqMar, iElMar);

            double dutyCyclePower = study.MainSystem.GetDutyCyclePower(options, configuration);

            return Json(dutyCyclePower, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }

        /// <summary>
        /// Returns the main system duty cycle dissipated power the IDM-CIC model associated to the specified session ID 
        /// with specific calculation options and for an IDM-CIC configuration.
        /// It always considers elements activation.
        /// If the configuration ID is not specified, it will use the IDM-CIC reference configuration.
        /// If the session ID is null or empty, uses the default IDM-CIC study of the server if it's not null.
        /// </summary>
        /// <param name="sessionId">The model session ID (default = null)</param>
        /// <param name="iEqMar">Include or not equipments power margin (default = false)</param>
        /// <param name="iElMar">Include or not elements power margin (default = false)</param>
        /// <param name="configurationId">The configuration ID (default = null)</param>
        /// <returns>JSON double</returns>
        [HttpGet]
        public IHttpActionResult GetDutyCycleDissipatedPower(string sessionId = null, bool iEqMar = false, bool iElMar = false, 
            string configurationId = null)
        {
            IdmStudy study = IdmCicHttpServer.Instance.GetIdmStudy(sessionId);

            Configuration configuration = ServiceHelper.getConfiguration(study, configurationId);
            MainSystemPowerOptions options = ServiceHelper.getPowerOptions(iEqMar, iElMar);

            double dutyCycleDissipation = study.MainSystem.GetDutyCycleDissipation(options, configuration);

            return Json(dutyCycleDissipation, ServiceHelper.getJsonSettings(), Encoding.UTF8);
        }
    }
}
