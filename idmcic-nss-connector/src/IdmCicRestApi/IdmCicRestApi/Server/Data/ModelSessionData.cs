﻿// License header goes here.
using IdmCic.API.Model;
using System.Threading;

namespace IdmCicRestApi.Server.Data
{
    /// <summary>
    /// Holds data for a model session of the server.
    /// </summary>
    public class ModelSessionData
    {
        /// <summary>
        /// The session ID
        /// </summary>
        public string SessionId { get; private set; }
        /// <summary>
        /// The loaded model path
        /// </summary>
        public string ModelPath { get; private set; }
        /// <summary>
        /// The loaded IDM study
        /// </summary>
        public IdmStudy IdmStudy { get; private set; }
        /// <summary>
        /// The session timer
        /// </summary>
        public Timer SessionTimer { get; private set; }

        /// <summary>
        /// Constructor of a ModelSessionData object
        /// </summary>
        /// <param name="sessionId">The session ID</param>
        /// <param name="modelPath">The loaded model path</param>
        /// <param name="idmStudy">The loaded IDM study</param>
        /// <param name="sessionTimer">The session timer</param>
        internal ModelSessionData(string sessionId, string modelPath, IdmStudy idmStudy, Timer sessionTimer)
        {
            SessionId = sessionId;
            ModelPath = modelPath;
            IdmStudy = idmStudy;
            SessionTimer = sessionTimer;
        }
    }
}
