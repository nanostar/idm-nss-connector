﻿// License header goes here.
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Threading;

namespace IdmCicRestApi.Server.Data
{
    /// <summary>
    /// Collection for server loaded model sessions.
    /// </summary>
    public class ModelSessionDataCollection : KeyedCollection<string, ModelSessionData>, INotifyCollectionChanged
    {
        /// <summary>
        /// The synchronization context helps the collection to be thread safe and send collection changed event in the creator thread.
        /// </summary>
        private SynchronizationContext _synchronizationContext = SynchronizationContext.Current;

        /// <summary>
        /// Event is fired when the collection is changed
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// The key for a model session item is it's session ID.
        /// </summary>
        /// <param name="item">The item</param>
        /// <returns>The item session ID</returns>
        protected override string GetKeyForItem(ModelSessionData item)
        {
            return item.SessionId;
        }

        /// <summary>
        /// Inserts an item to the collection.
        /// </summary>
        /// <param name="index">Item index</param>
        /// <param name="item">Item to insert</param>
        protected override void InsertItem(int index, ModelSessionData item)
        {
            base.InsertItem(index, item);

            collectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Replace an item by a new one.
        /// </summary>
        /// <param name="index">Item index</param>
        /// <param name="item">New item</param>
        protected override void SetItem(int index, ModelSessionData item)
        {
            ModelSessionData replacedItem = Items[index];

            if (replacedItem != null)
            {
                // Release the timer
                replacedItem.SessionTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }

            base.SetItem(index, item);

            collectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, replacedItem));
        }

        /// <summary>
        /// Removes an item from the collection.
        /// </summary>
        /// <param name="index">Item index</param>
        protected override void RemoveItem(int index)
        {
            ModelSessionData removedItem = Items[index];

            // Release the timer
            removedItem.SessionTimer.Change(Timeout.Infinite, Timeout.Infinite);

            base.RemoveItem(index);

            collectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItem));
        }

        /// <summary>
        /// Clear all items from the collection.
        /// </summary>
        protected override void ClearItems()
        {
            // Release all timers
            foreach (ModelSessionData item in this)
            {
                item.SessionTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }

            base.ClearItems();

            collectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Fire the collection changed event ensuring the event to be fire in the right synchronization context.
        /// </summary>
        /// <param name="args">Event arguments</param>
        private void collectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (SynchronizationContext.Current == _synchronizationContext)
            {
                // Execute the CollectionChanged event on the current thread
                RaiseCollectionChanged(args);
            }
            else
            {
                // Raises the CollectionChanged event on the creator thread
                _synchronizationContext.Send(RaiseCollectionChanged, args);
            }
        }

        /// <summary>
        /// Fire the collection changed event.
        /// </summary>
        /// <param name="param">Event arguments</param>
        private void RaiseCollectionChanged(object param)
        {
            // We are in the creator thread, call the base implementation directly
            CollectionChanged?.Invoke(this, (NotifyCollectionChangedEventArgs)param);
        }
    }
}
