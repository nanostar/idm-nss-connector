﻿// License header goes here.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;

namespace IdmCicRestApi.Server.Data
{
    /// <summary>
    /// Collection for server exposed IDM files.
    /// </summary>
    public class ExposedFileDataCollection : KeyedCollection<string, ExposedFileData>, INotifyCollectionChanged
    {
        /// <summary>
        /// The synchronization context helps the collection to be thread safe and send collection changed event in the creator thread.
        /// </summary>
        private SynchronizationContext _synchronizationContext = SynchronizationContext.Current;

        /// <summary>
        /// Event is fired when the collection is changed
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// The key for an exposed data file item is it's file path.
        /// </summary>
        /// <param name="item">The item</param>
        /// <returns>The item file path</returns>
        protected override string GetKeyForItem(ExposedFileData item)
        {
            return item.FilePath;
        }

        /// <summary>
        /// Inserts an item to the collection.
        /// </summary>
        /// <param name="index">Item index</param>
        /// <param name="item">Item to insert</param>
        protected override void InsertItem(int index, ExposedFileData item)
        {
            base.InsertItem(index, item);

            collectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Replace an item by a new one.
        /// </summary>
        /// <param name="index">Item index</param>
        /// <param name="item">New item</param>
        protected override void SetItem(int index, ExposedFileData item)
        {
            ExposedFileData replacedItem = Items[index];

            base.SetItem(index, item);

            collectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, replacedItem));
        }

        /// <summary>
        /// Removes an item from the collection.
        /// </summary>
        /// <param name="index">Item index</param>
        protected override void RemoveItem(int index)
        {
            ExposedFileData removedItem = Items[index];

            base.RemoveItem(index);

            collectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItem));
        }

        /// <summary>
        /// Clear all items from the collection.
        /// </summary>
        protected override void ClearItems()
        {
            base.ClearItems();

            collectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Fire the collection changed event ensuring the event to be fire in the right synchronization context.
        /// </summary>
        /// <param name="args">Event arguments</param>
        private void collectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (SynchronizationContext.Current == _synchronizationContext)
            {
                // Execute the CollectionChanged event on the current thread
                RaiseCollectionChanged(args);
            }
            else
            {
                // Raises the CollectionChanged event on the creator thread
                _synchronizationContext.Send(RaiseCollectionChanged, args);
            }
        }

        /// <summary>
        /// Fire the collection changed event.
        /// </summary>
        /// <param name="param">Event arguments</param>
        private void RaiseCollectionChanged(object param)
        {
            // We are in the creator thread, call the base implementation directly
            CollectionChanged?.Invoke(this, (NotifyCollectionChangedEventArgs)param);
        }

        /// <summary>
        /// Convert the collection to a dictionary.
        /// For each item, the key is the file path and the value is the custom name.
        /// </summary>
        /// <returns>The collection as dictionary</returns>
        public Dictionary<string, string> ToDictionary()
        {
            return this.ToDictionary(t => t.FilePath, t => t.CustomName);
        }

        /// <summary>
        /// Update the collection from a dictionary in which, for each item, the key is the file path and
        /// the value is the custom name.
        /// All items of the collections are previously cleared.
        /// </summary>
        /// <param name="dictionary">The dictionary to insert</param>
        /// <exception cref="ArgumentException">Thrown if the specified dictionary is null</exception>
        public void FromDictionary(Dictionary<string, string> dictionary)
        {
            if (dictionary == null)
            {
                throw new ArgumentException("The specified dictionary can't be null");
            }
            Clear();

            foreach (KeyValuePair<string, string> kvp in dictionary)
            {
                Add(new ExposedFileData(kvp.Key, kvp.Value));
            }
        }
    }
}
