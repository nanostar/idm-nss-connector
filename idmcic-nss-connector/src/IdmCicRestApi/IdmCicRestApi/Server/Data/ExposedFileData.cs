﻿// License header goes here.
namespace IdmCicRestApi.Server.Data
{
    /// <summary>
    /// Holds data for an IDM file exposed by the server.
    /// </summary>
    public class ExposedFileData
    {
        /// <summary>
        /// The file path
        /// </summary>
        public string FilePath { get; private set; }
        /// <summary>
        /// The custom name
        /// </summary>
        public string CustomName { get; set; }

        /// <summary>
        /// Constructor of an ExposedFileData object.
        /// </summary>
        /// <param name="filePath">The file path</param>
        /// <param name="customName">The custom name</param>
        public ExposedFileData(string filePath, string customName)
        {
            FilePath = filePath;
            CustomName = customName;
        }
    }
}
