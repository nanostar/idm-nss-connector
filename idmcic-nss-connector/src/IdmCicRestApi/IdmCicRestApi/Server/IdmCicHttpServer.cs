﻿// License header goes here.
using IdmCic.API.Model;
using IdmCicRestApi.Server.Data;
using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.SelfHost;

namespace IdmCicRestApi.Server
{
    /// <summary>
    /// This class is a singleton that initialise an IDM-CIC HTTP server to expose the IDM-CIC web service controller methods.
    /// It allows to load models through HTTP requests, store them with a unique session for each loaded models and parse
    /// the model or call some functions using models session ID.
    /// It can also hold a default IDM-CIC model so a service can retrieve this model without specifying a session ID.
    /// It can be usefull for an application that uses a single model and want to expose only this model.
    /// </summary>
    public class IdmCicHttpServer : INotifyPropertyChanged
    {
        #region Singleton

        /// <summary>
        /// Holds the singleton instance if the IDM-CIC HTTP server.
        /// </summary>
        private static IdmCicHttpServer instance;

        /// <summary>
        /// Gets the singleton instance of the IDM-CIC HTTP server.
        /// </summary>
        public static IdmCicHttpServer Instance
        {
            get
            {
                if (instance == null)
                {
                    // Create the instance
                    instance = new IdmCicHttpServer();
                }

                return instance;
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// The self host HTTP server
        /// </summary>
        private HttpSelfHostServer server;

        /// <summary>
        /// Holds the port to open by the server.
        /// </summary>
        private int port = 80;

        /// <summary>
        /// Holds the collection of exposed models.
        /// </summary>
        private ExposedFileDataCollection exposedIdmFiles = new ExposedFileDataCollection();

        /// <summary>
        /// Holds a flag indicating if the server allows to load only exposed IDM files or any local IDM file.
        /// </summary>
        private bool restrictToExposedModels = false;

        /// <summary>
        /// Holds the models session life time.
        /// An IDM-CIC model will be stored in memory and associated with a session GUID during this amount of time.
        /// <b>Warning:</b> The lifetime is stored in milliseconds in this field while the associated property is 
        /// specified in seconds (so it can be used within timers).
        /// </summary>
        private int modelsSessionsLifeTime = 900 * 1000;

        /// <summary>
        /// Holds all currently loaded models sessions.
        /// </summary>
        private ModelSessionDataCollection modelsSessions = new ModelSessionDataCollection();

        /// <summary>
        /// Holds the default IDM-CIC study that can be used by the service when a function is called without a session ID.
        /// </summary>
        private IdmStudy defaultStudy;

        /// <summary>
        /// Event occuring when a property is changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public properties

        /// <summary>
        /// Get / Set the port to be opened by the server.
        /// Setting the port when server is started is forbidden.
        /// </summary>
        /// <exception cref="ApplicationException">Thrown if the server is started</exception>
        /// <exception cref="ArgumentException">Thrown if the specified value is lower or equel to 0</exception>
        public int Port
        {
            get { return port; }
            set
            {
                if (value != port)
                {
                    if (IsStarted())
                    {
                        throw new ApplicationException("The server must be stopped to change the port");
                    }

                    if (value <= 0)
                    {
                        throw new ArgumentException("The port must be greater than 0");
                    }

                    port = value;

                    onPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Get / Set the dictionary of exposed models.
        /// </summary>
        /// <exception cref="ArgumentNullException">Thrown if the specified value is null</exception>
        public ExposedFileDataCollection ExposedIdmFiles
        {
            get { return exposedIdmFiles; }
        }

        /// <summary>
        /// Get / Set a flag indicating if the server allows to load only exposed IDM files or any local IDM file.
        /// </summary>
        public bool RestrictToExposedModels
        {
            get { return restrictToExposedModels; }
            set
            {
                if (value != restrictToExposedModels)
                {
                    restrictToExposedModels = value;

                    onPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the collection of current model sessions.
        /// </summary>
        public ModelSessionDataCollection ModelsSessions
        {
            get { return modelsSessions; }
        }

        /// <summary>
        /// Get / Set the models session life time in seconds.
        /// An IDM-CIC model will be stored in memory and associated with a session GUID during this amount of time.
        /// Setting this property automatically reinitialise all sessions timers if the specified value is different than current value.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown if the specified value is lower or equal to 0</exception>
        public int ModelsSessionsLifeTime
        {
            get { return modelsSessionsLifeTime / 1000; }
            set
            {
                if (value != ModelsSessionsLifeTime)
                {
                    if (value <= 0)
                    {
                        throw new ArgumentException("The sessions lifetime must be greater than 0");
                    }

                    modelsSessionsLifeTime = value * 1000;

                    // Models session is converted to generic list to ensure that if the collection is changed due to a short session lifetime, the loop does not break.
                    foreach (ModelSessionData session in ModelsSessions.ToList())
                    {
                        session.SessionTimer.Change(modelsSessionsLifeTime, 5);
                    }

                    onPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Get / Set the default study that can be used by the service when not specifying a session ID.
        /// </summary>
        public IdmStudy DefaultStudy
        {
            get { return defaultStudy; }
            set
            {
                if (value != defaultStudy)
                {
                    defaultStudy = value;

                    onPropertyChanged();
                }
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Starts the server.
        /// Using this method is not allowed if the server is already started.
        /// </summary>
        /// <exception cref="ApplicationException">Thrown if the server is already started</exception>
        public void Start()
        {
            if (server != null)
            {
                throw new ApplicationException("Server already started");
            }

            server = new HttpSelfHostServer(getServerConfiguration());

            // Start listening
            try
            {
                server.OpenAsync().Wait();
            }
            catch
            {
                // Set the server to null to ensure it's not specified as started
                server = null;
                throw;
            }
        }

        /// <summary>
        /// Stops the server releasing all models session if needed.
        /// Using this method is not allowed if the server is not started.
        /// </summary>
        /// <param name="releaseAllSessions">Release or not all currently loaded models sessions (default = true)</param>
        /// <exception cref="ApplicationException">Thrown if the server is not started</exception>
        public void Stop(bool releaseAllSessions = true)
        {
            if (server == null)
            {
                throw new ApplicationException("Server not started");
            }

            if (releaseAllSessions)
            {
                ModelsSessions.Clear();
            }

            // Stop listening
            server.CloseAsync().Wait();

            // Set the server object to null (allows to know that the server is not started)
            server = null;
        }

        /// <summary>
        /// Returns a flag indicating if the server is actually opened.
        /// </summary>
        /// <returns>True if server is started, else false</returns>
        public bool IsStarted()
        {
            return server != null;
        }
        
        /// <summary>
        /// Loads an IDM model and create a new session for this model, initialising the associated timer.
        /// </summary>
        /// <param name="modelPath">The path of the IDM file to load</param>
        /// <param name="checkRestricted">Check if restricted mode is enabled and, if so, if specified model isz in exposed models</param>
        /// <returns>The new session ID</returns>
        /// <exception cref="ArgumentException">Thrown if the server is restricted to exposed models and the specified 
        /// path of the IDM file is not exposed</exception>
        public ModelSessionData LoadIdmModel(string modelPath, bool checkRestricted = true)
        {
            if (checkRestricted && RestrictToExposedModels && !exposedIdmFiles.Contains(modelPath))
            {
                throw new ArgumentException("The specified IDM file is not exposed by the server");
            }

            // Load the IDM-CIC study
            IdmStudy loadedStudy = new IdmStudy(modelPath);

            // Create a new session ID from a GUID (lower case!)
            string sessionId = Guid.NewGuid().ToString().ToLower();

            // Initialise the timer
            Timer timer = new Timer(delegate (object stateInfo)
            {
                ModelsSessions.Remove(sessionId);
            }, null, modelsSessionsLifeTime, 5);

            // Create the model session and store it
            ModelSessionData newModelSessionData = new ModelSessionData(sessionId, modelPath, loadedStudy, timer);
            ModelsSessions.Add(newModelSessionData);

            // Return the session ID
            return newModelSessionData;
        }

        /// <summary>
        /// Returns the IDM study associated to the specified session ID and reinitialise the associated timer with the models 
        /// session lifetime if needed.
        /// </summary>
        /// <param name="sessionId">The session ID</param>
        /// <param name="reinitialiseTimer">Reinitialise or not the session timer (default = true)</param>
        /// <returns>The IDM-CIC study for the specified session ID or the default study if session ID is not specified</returns>
        /// <exception cref="ArgumentException">Thrown if no study can be returned for specified parameters</exception>
        public IdmStudy GetIdmStudy(string sessionId = null, bool reinitialiseTimer = true)
        {
            if (string.IsNullOrEmpty(sessionId))
            {
                if (DefaultStudy == null)
                {
                    throw new ArgumentException("The server doesn't expose a default IDM-CIC study");
                }

                return DefaultStudy;
            }

            // Lower case the specified session ID (always stored in lower-case)
            sessionId = sessionId.ToLower();

            if (!ModelsSessions.Contains(sessionId))
            {
                // Model session does not exist
                throw new ArgumentException("IDM-CIC model session not found or expired for session id " + sessionId);
            }

            ModelSessionData session = ModelsSessions[sessionId];

            if (reinitialiseTimer)
            {
                // Reinitialise the session timer
                session.SessionTimer.Change(modelsSessionsLifeTime, 5);
            }

            // Return the IDM study
            return session.IdmStudy;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Fire a property changed event for a property.
        /// </summary>
        /// <param name="propertyName">The property name (if not specified, will be the calling member)</param>
        private void onPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Returns the configuration used to start the HTTP server.
        /// </summary>
        /// <returns></returns>
        private HttpSelfHostConfiguration getServerConfiguration()
        {
            // Get the base address
            Uri baseAddress = new Uri("http://localhost:" + Port + "/");

            // Create the configuration used to start the server
            HttpSelfHostConfiguration config = new HttpSelfHostConfiguration(baseAddress);

            // Route to the controller
            config.Routes.MapHttpRoute(name: "API", routeTemplate: "{controller}/{action}");

            // Add message handler to log service calls
            config.MessageHandlers.Add(new ServerMessageHandler());

            // Enable CORS for all origin, all headers and all methods so it can be used in AJAX everywhere
            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Default JSON response formatting
            config.Formatters.JsonFormatter.Indent = true;
            config.Formatters.JsonFormatter.SupportedEncodings.Add(Encoding.UTF8);

            // Add new exception filter to return service exceptions to client as JSON
            config.Filters.Add(new ServerExceptionFilterAttribute());

            // Return the config
            return config;
        }

        #endregion
    }
}
