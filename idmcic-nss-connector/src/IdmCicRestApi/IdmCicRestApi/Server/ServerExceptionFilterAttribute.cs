﻿// License header goes here.
using IdmCicRestApi.Helpers;
using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace IdmCicRestApi.Server
{
    /// <summary>
    /// This class is used to handle exceptions from server HTTP request so they can be logged and returned 
    /// properly serialized in JSON with InternalServerError code .
    /// </summary>
    internal class ServerExceptionFilterAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// Catch exception to log it and serialize it for returning a custom response.
        /// </summary>
        /// <param name="context">The context for the action.</param>
        public override void OnException(HttpActionExecutedContext context)
        {
            // Log error
            string message = ServiceHelper.getLogMessageFromRequest(context.Request) + " failed";
            LogManager.GetLogger("ServiceErrors").Error(context.Exception, message);

            // Create custom response
            context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, 
                serializeException(context.Exception), MediaTypeHeaderValue.Parse("application/json"));
        }
        
        /// <summary>
        /// Serialize an Exception to a dictionary including inner exceptions.
        /// This dictionary can be easily serialized to JSON.
        /// </summary>
        /// <param name="e">The Exception to serialize.</param>
        /// <returns>The Exception serialized as Dictionary.</returns>
        private static Dictionary<string, object> serializeException(Exception e)
        {
            Dictionary<string, object> returnedDic = new Dictionary<string, object>();

            returnedDic.Add("ErrorType", e.GetType().FullName);
            returnedDic.Add("Message", e.Message);
            returnedDic.Add("StackTrace", e.StackTrace);

            if (e.InnerException != null)
            {
                // Recursive call of the function
                returnedDic.Add("InnerException", serializeException(e.InnerException));
            }

            return returnedDic;
        }
    }
}
