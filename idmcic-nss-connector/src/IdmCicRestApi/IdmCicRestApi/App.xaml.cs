﻿// License header goes here.
using System.Globalization;
using System.Threading;
using System.Windows;

namespace IdmCicRestApi
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        MainWindow wnd;

        /// <summary>
        /// Starts the application and opens the main window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // Set the culture to invariant culture
            CultureInfo.DefaultThreadCurrentUICulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;

            wnd = new MainWindow();
            
            // Open the window
            wnd.Show();
        }
        
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            wnd.AddException(e.Exception);

            e.Handled = true;
        }
    }
}
