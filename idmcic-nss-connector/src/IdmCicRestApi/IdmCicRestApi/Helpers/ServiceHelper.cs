﻿// License header goes here.
using IdmCic.API.Model;
using IdmCic.API.Model.Configurations;
using IdmCic.API.Model.Physics;
using IdmCic.API.Model.Power;
using IdmCic.API.Model.Subsystems;
using IdmCic.API.Utils.Calculation.MCI;
using IdmCic.API.Utils.Calculation.Power;
using IdmCic.API.Utils.Collections.COM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Http;
using System.Reflection;
using System.ServiceModel.Channels;
using System.Web;

namespace IdmCicRestApi.Helpers
{
    /// <summary>
    /// This static class expose usefull functions used by the IDM-CIC web service controller.
    /// </summary>
    internal static class ServiceHelper
    {
        /// <summary>
        /// These JSON serializer settings shall be used by the service controller methods to return formatted JSON
        /// </summary>
        private static JsonSerializerSettings jsonSettings;

        /// <summary>
        /// Returns the JSON settings that shall be used by service controller methods to return formatted JSON response.
        /// Initialise the "jsonSettings" field if isz actually null.
        /// </summary>
        /// <returns></returns>
        internal static JsonSerializerSettings getJsonSettings()
        {
            if (jsonSettings == null)
            {
                jsonSettings = new JsonSerializerSettings();
                jsonSettings.Formatting = Formatting.Indented;
                jsonSettings.NullValueHandling = NullValueHandling.Include;
            }

            return jsonSettings;
        }

        /// <summary>
        /// Get the client IP address as string from a handled request.
        /// </summary>
        /// <param name="request">The request from which to retrieve the client IP address</param>
        /// <returns>The caller IP address</returns>
        internal static string getClientIpFromRequest(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                RemoteEndpointMessageProperty prop = (RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name];
                return prop.Address == "::1" ? "localhost" : prop.Address;
            }
            else if (HttpContext.Current != null)
            {
                return HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get a standard log message for an incoming HTTP request.
        /// </summary>
        /// <param name="request">The request</param>
        /// <returns>A standard log message</returns>
        internal static string getLogMessageFromRequest(HttpRequestMessage request)
        {
            return string.Format("[{0}] {1} received from {2}",
                request.Method.ToString(),
                request.RequestUri.ToString(),
                getClientIpFromRequest(request));
        }

        #region IDM-CIC helpers functions

        /// <summary>
        /// Can be used by service controller methods to get needed mass options for calculations according to retrieved parameters as boolean.
        /// Always consider elements activation and tank contents.
        /// </summary>
        /// <param name="iEqMar">Include or not equipments mass margin</param>
        /// <param name="iElMar">Include or not elements mass margin</param>
        /// <param name="iSysProMar">Include or not system propellant mass margin</param>
        /// <returns>Mass options to use for a calculation</returns>
        internal static MainSystemMassOptions getMassOptions(bool iEqMar, bool iElMar, bool iSysProMar)
        {
            MainSystemMassOptions options = MainSystemMassOptions.IncludeElementActivation | MainSystemMassOptions.IncludeTankContent;

            if (iEqMar)
            {
                options |= MainSystemMassOptions.IncludeEquipmentMargin;
            }
            if (iElMar)
            {
                options |= MainSystemMassOptions.IncludeElementMargin;
            }
            if (iSysProMar)
            {
                options |= MainSystemMassOptions.IncludeSystemPropellantMargin;
            }

            return options;
        }

        /// <summary>
        /// Can be used by service controller methods to get needed power options for calculations according to retrieved parameters as boolean.
        /// Always consider elements activation.
        /// </summary>
        /// <param name="iEqMar">Include or not equipments power margin</param>
        /// <param name="iElMar">Include or not elements power margin</param>
        /// <returns>Power options to use for a calculation</returns>

        internal static MainSystemPowerOptions getPowerOptions(bool iEqMar, bool iElMar)
        {
            MainSystemPowerOptions options = MainSystemPowerOptions.IncludeElementActivation;

            if (iEqMar)
            {
                options |= MainSystemPowerOptions.IncludeEquipmentMargin;
            }
            if (iElMar)
            {
                options |= MainSystemPowerOptions.IncludeElementMargin;
            }

            return options;
        }

        /// <summary>
        /// Retrieve an IDM-CIC configuration from a study according to its ID and throw an exception if configuration does not exists 
        /// within the system.
        /// </summary>
        /// <param name="study">The IDM-CIC study in which to retrieve the configuration</param>
        /// <param name="configurationId">The ID of the configuration to retrieve</param>
        /// <returns>The retrieved IDM-CIC configuration</returns>
        /// <exception cref="KeyNotFoundException">Thrown if the configuration does not exists within the system</exception>
        internal static Configuration getConfiguration(IdmStudy study, string configurationId)
        {
            if (string.IsNullOrEmpty(configurationId))
            {
                return null;
            }
            else
            {
                Configuration configuration = study.MainSystem.GetConfiguration(configurationId);

                if (configuration == null)
                {
                    throw new KeyNotFoundException("The configuration with ID " + configurationId + " does not exists with the system");
                }

                return configuration;
            }
        }

        /// <summary>
        /// Retrieve an IDM-CIC system mode from a study according to its ID and throw an exception if system mode does not exists 
        /// within the system.
        /// </summary>
        /// <param name="study">The IDM-CIC study in which to retrieve the system mode</param>
        /// <param name="systemModeId">The ID of the system mode to retrieve</param>
        /// <returns>The retrieved IDM-CIC system mode</returns>
        /// <exception cref="KeyNotFoundException">Thrown if the system mode does not exists within the system</exception>
        internal static SystemMode getSystemMode(IdmStudy study, string systemModeId)
        {
            if (string.IsNullOrEmpty(systemModeId))
            {
                return null;
            }
            else
            {
                SystemMode systemMode = study.MainSystem.GetSystemMode(systemModeId);

                if (systemMode == null)
                {
                    throw new KeyNotFoundException("The system mode with ID " + systemModeId + " does not exists with the system");
                }

                return systemMode;
            }
        }

        #endregion

        #region IDM objects serialization

        #region Properties filters

        /// <summary>
        /// A list of property filters to be used when parsing an IDM object by reflexion.
        /// Initialized automatically when class is called.
        /// </summary>
        private static List<PropertyFilter> propertiesToFilter = initialisePropertiesToFilter();

        /// <summary>
        /// Initialise property filters to be used when parsing an IDM object by reflexion.
        /// </summary>
        /// <returns></returns>
        private static List<PropertyFilter> initialisePropertiesToFilter()
        {
            // These filters ensure that some objects are filtered because it's been detected that they can raise exception when parsing their properties
            List<PropertyFilter> filters = new List<PropertyFilter>();

            filters.Add(getPropertyFilter((TranslationValue a) => a.Translation));
            filters.Add(getPropertyFilter((RotationValue a) => a.Rotation));
            filters.Add(getPropertyFilter((TankContent a) => a.DefaultPropellantShape));
            filters.Add(getPropertyFilter((TankContentValue a) => a.PropellantShape));
            filters.Add(getPropertyFilter((ArticulationValue a) => a.Position));
            filters.Add(getPropertyFilter((Articulation a) => a.DefaultPosition));

            return filters;
        }

        /// <summary>
        /// This function automatically creates a property filter based on an expression for a member access and returns it.
        /// The usage of this function is then GetPropertyFilter((ObjectType t) => t.MemberName).
        /// </summary>
        /// <typeparam name="T">Type of object concerned</typeparam>
        /// <typeparam name="TValue">The member call</typeparam>
        /// <param name="memberAccess">The member access expression for which to retrieve the name</param>
        /// <returns>A property filter for the concerned object type and member</returns>
        private static PropertyFilter getPropertyFilter<T, TValue>(Expression<Func<T, TValue>> memberAccess)
        {
            MemberExpression expression = (MemberExpression)memberAccess.Body;
            return new PropertyFilter(expression.Member.DeclaringType, expression.Member.Name);
        }

        #endregion

        /// <summary>
        /// Serialize an IDM object in a dictionary that will contains all its public properties, children and referenced objects.
        /// Childs can be held as reference (Dictionary with a single item, "FullId"=object_full_id) or as fully serialized objects (recursive call of this function).
        /// For each item in the dictionary, the key is the property name and the value is:
        /// - The property value if type is primitive or string
        /// - The IDM object serialized as reference or fully if it's a child and the getChildsAsReference parameter is true
        /// - A list of IDM objects serialized as reference or fully if they're children and the getChildsAsReference parameter is true
        /// 
        /// The returned dictionary can then be fully and easily serialized in JSON by the service controller methods.
        /// </summary>
        /// <param name="idmObject">The IDM object to serialize</param>
        /// <returns>A dictionary that can be serialized in JSON</returns>
        internal static Dictionary<string, object> getIdmObjectAsDictionary(IdmObject idmObject, bool getChildsAsReference)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            // Loop on properties by reflection
            foreach (PropertyInfo propertyInfo in idmObject.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                // Try to find a filter for this property
                PropertyFilter filterForProperty = propertiesToFilter.Find(o => o.Evaluate(propertyInfo));

                if (filterForProperty == null)
                {
                    // The property is not excluded by filters

                    // Flag used to know if property can be treated
                    bool treatProperty = true;
                    // Flag used to know if property is primitive (or string)
                    bool isPrimitive = false;
                    // Flag used to know if property is IDM object
                    bool isIdmObject = false;
                    // Flag used to know if property is an IDM enumerable (IdmList or IdmDictionary)
                    bool isIdmEnumerable = false;

                    if (typeIsSimple(propertyInfo.PropertyType))
                    {
                        isPrimitive = true;
                    }
                    else if (typeof(IdmObject).IsAssignableFrom(propertyInfo.PropertyType))
                    {
                        isIdmObject = true;
                    }
                    else if (typeof(_IdmList).IsAssignableFrom(propertyInfo.PropertyType) ||
                        typeof(_IdmDictionary).IsAssignableFrom(propertyInfo.PropertyType))
                    {
                        isIdmEnumerable = true;
                    }
                    else
                    {
                        // Do not treat this property as it's not usable (specific object visible for the .NET API for example)
                        treatProperty = false;
                    }

                    if (treatProperty)
                    {
                        filterForProperty = propertiesToFilter.Find(o => o.Evaluate(propertyInfo));

                        // Get the value
                        object propertyValue = propertyInfo.GetValue(idmObject);

                        if (propertyValue != null)
                        {
                            if (isPrimitive)
                            {
                                // Send the property value itself
                                properties.Add(propertyInfo.Name, propertyValue);
                            }
                            else if (isIdmObject)
                            {
                                IdmObject currentObject = (IdmObject)propertyValue;

                                if (!getChildsAsReference && currentObject.Parent == idmObject)
                                {
                                    // Full serialization of the child object
                                    properties.Add(propertyInfo.Name, getIdmObjectAsDictionary(currentObject, false));
                                }
                                else
                                {
                                    // Serialize the object as reference
                                    properties.Add(propertyInfo.Name, getIdmObjectAsReference(currentObject));
                                }
                            }
                            else if (isIdmEnumerable)
                            {
                                // List of serialized IDM objects
                                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

                                foreach (IdmObject currentObject in (IEnumerable<IdmObject>)propertyValue)
                                {
                                    if (!getChildsAsReference && currentObject.Parent == idmObject)
                                    {
                                        // Full serialization of the child object
                                        list.Add(getIdmObjectAsDictionary(currentObject, false));
                                    }
                                    else
                                    {
                                        // Serialize the object as reference
                                        list.Add(getIdmObjectAsReference(currentObject));
                                    }
                                }

                                properties.Add(propertyInfo.Name, list);
                            }
                        }
                    }
                }
            }

            return properties;
        }

        /// <summary>
        /// Returns an IDM object serialized as reference within a dictionary with a single item.
        /// The key is "FullId" and the value is the object full ID.
        /// </summary>
        /// <param name="idmObject">The IDM object to serialize</param>
        /// <returns>A dictionary that can be serialized in JSON</returns>
        private static Dictionary<string, object> getIdmObjectAsReference(IdmObject idmObject)
        {
            Dictionary<string, object> returnedDic = new Dictionary<string, object>();
            returnedDic.Add("FullId", idmObject.FullId);
            return returnedDic;
        }

        /// <summary>
        /// Gets a flag indicating if a type is "simple", which means that it's a primitive type, a string, a decimal, 
        /// any nullable of these types or an array of "simple" type.
        /// If so, it can be used when serializing an IDM object to a dictionary.
        /// </summary>
        /// <param name="type">The type to check</param>
        /// <returns>A flag indicating if the type can be used within a serialized IDM object</returns>
        private static bool typeIsSimple(Type type)
        {
            if (type.IsArray)
            {
                return typeIsSimple(type.GetElementType());
            }

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // nullable type, check if the nested type is simple.
                return typeIsSimple(type.GetGenericArguments()[0]);
            }

            return type.IsPrimitive
              || type.IsEnum
              || type.Equals(typeof(string))
              || type.Equals(typeof(decimal));
        }
        
        #endregion
    }
}
