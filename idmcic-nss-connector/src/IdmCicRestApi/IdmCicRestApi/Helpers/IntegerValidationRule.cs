﻿// License header goes here.
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace IdmCicRestApi.Helpers
{
    /// <summary>
    /// Validation rule to ensure that a textbox value is a positive integer not starting by 0.
    /// </summary>
    public class IntegerValidationRule : ValidationRule
    {
        /// <summary>
        /// This regex helps to ensure that the value passed to the validation is a positive integer not starting by 0.
        /// </summary>
        private static readonly Regex integerRegex = new Regex("^[1-9][0-9]*$");

        /// <summary>
        /// Validates the value.
        /// </summary>
        /// <param name="value">The value to check</param>
        /// <param name="cultureInfo">The culture info to use</param>
        /// <returns>A validation result object</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var str = value as string;

            if (str == null || !integerRegex.IsMatch(str))
            {
                return new ValidationResult(false, "Value must be a positive integer greater than 0");
            }

            return new ValidationResult(true, null);
        }
    }
}
