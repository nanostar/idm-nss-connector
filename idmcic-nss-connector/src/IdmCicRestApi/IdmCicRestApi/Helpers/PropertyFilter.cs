﻿// License header goes here.
using System;
using System.Reflection;

namespace IdmCicRestApi.Helpers
{
    /// <summary>
    /// Identifies a property filter to be used when parsing objects by reflection to serialized them.
    /// </summary>
    internal class PropertyFilter
    {
        /// <summary>
        /// The type of object holding the property
        /// </summary>
        public Type Type
        {
            get;
            set;
        }

        /// <summary>
        /// The name of the property
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        internal PropertyFilter(Type type, string name)
        {
            Type = type;
            Name = name;
        }

        /// <summary>
        /// This method evaluates wether a property shall befiltered or not.
        /// 
        /// Returns true if the property should be filtered according to its name and the object type 
        /// </summary>
        /// <param name="propertyInfo">The property to consider</param>
        /// <returns>true if the property should be filtered, else false</returns>
        internal bool Evaluate(PropertyInfo propertyInfo)
        {
            if ((Name == "*" || propertyInfo.Name == Name) && 
                (Type == null || Type.IsAssignableFrom(propertyInfo.DeclaringType)))
            {
                return true;
            }

            return false;
        }
    }
}
