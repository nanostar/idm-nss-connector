﻿// License header goes here.
using System;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace IdmCicRestApi
{
    /// <summary>
    /// Startup program that is written to ensure that main application find its referenced libraries not in the folder but in embed resources.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Starts the program
        /// </summary>
        /// <param name="args"></param>
	    [STAThread]
        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve += OnResolveAssembly;

            App.Main();
        }
        
        /// <summary>
        /// Find and load needed assemblies within the application own manifest resource
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static Assembly OnResolveAssembly(object sender, ResolveEventArgs args)
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = new AssemblyName(args.Name);

            string path = assemblyName.Name;
            
            if (assemblyName.CultureInfo.Equals(CultureInfo.InvariantCulture) == false)
            {
                path = string.Format(@"{0}\{1}", assemblyName.CultureInfo, path);
            }

            Stream stream = executingAssembly.GetManifestResourceStream(path + ".dll");

            if (stream != null)
            {
                byte[] assemblyRawBytes = new byte[stream.Length];
                stream.Read(assemblyRawBytes, 0, assemblyRawBytes.Length);
                return Assembly.Load(assemblyRawBytes);
            }

            return null;
        }
        
    }
}
