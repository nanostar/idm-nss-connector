﻿// License header goes here.
using IdmCic.API.Model;
using IdmCic.API.Service.Parameters;
using IdmCicRestApi.Server;
using IdmCicRestApi.Server.Data;
using Microsoft.Win32;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace IdmCicRestApi
{
    /// <summary>
    /// This window helps to manage the IDM-CIC HTTP server singleton.
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constants

        /// <summary>
        /// The application name for the IDM-CIC parameters manager
        /// </summary>
        private const string PARAMETERS_MANAGER_APPLICATION_NAME = "IdmCicWebService";
        /// <summary>
        /// The parameter name for the server port
        /// </summary>
        private const string SERVER_PORT_PARAMETER = "serverPort";
        /// <summary>
        /// The parameter name for the models sessions life time
        /// </summary>
        private const string SESSIONS_LIFETIME_PARAMETER = "modelsSessionsLifetime";
        /// <summary>
        /// The parameter name for the exposed IDM files
        /// </summary>
        private const string EXPOSED_IDM_FILES_PARAMETER = "exposedIdmFiles";
        /// <summary>
        /// The parameter name for the flag that allows to restrict models to those exposed by the server
        /// </summary>
        private const string RESTRICT_TO_EXPOSED_MODELS_PARAMETER = "restrictToExposedModels";
        /// <summary>
        /// The parameter name for the flag that helps to automatically start the HTTP server when the application starts
        /// </summary>
        private const string AUTO_START_SERVER_PARAMETER = "autoStartServer";
        /// <summary>
        /// The parameter name for the width of the window when closed in the last session
        /// </summary>
        private const string LAST_WIDTH_PARAMETER = "lastWidth";
        /// <summary>
        /// The parameter name for the height of the window when closed in the last session
        /// </summary>
        private const string LAST_HEIGHT_PARAMETER = "lastHeight";
        /// <summary>
        /// The parameter name for the window state when closed in the last session
        /// </summary>
        private const string LAST_WINDOW_STATE = "windowState";

        #endregion

        #region Fields

        /// <summary>
        /// An IDM-CIC parameters manager that helps to stores the server saved parameters.
        /// </summary>
        private ParametersManager parametersManager = new ParametersManager(PARAMETERS_MANAGER_APPLICATION_NAME);

        #endregion

        #region MainWindow 
        
        /// <summary>
        /// Constructor of the window.
        /// Initialise all needed parameters in the window (port, models sessions life time and exposed IDM files) and automatically
        /// start the HTTP server if specified by the corresponding parameter.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Get default errors brush
            defaultErrorsHeaderBrush = errorsTabHeader_textBlock.Foreground;

            // Retrieve all parameters
            int listeningPort = parametersManager.GetParameter(SERVER_PORT_PARAMETER, IdmCicHttpServer.Instance.Port);
            int sessionsLifetime = parametersManager.GetParameter(SESSIONS_LIFETIME_PARAMETER, IdmCicHttpServer.Instance.ModelsSessionsLifeTime);
            string exposedIdmFilesAsString = parametersManager.GetParameter(EXPOSED_IDM_FILES_PARAMETER, JsonConvert.SerializeObject(IdmCicHttpServer.Instance.ExposedIdmFiles.ToDictionary()));
            Dictionary<string, string> exposedIdmFiles = JsonConvert.DeserializeObject<Dictionary<string, string>>(exposedIdmFilesAsString);
            bool restrictToExposedModels = parametersManager.GetParameter(RESTRICT_TO_EXPOSED_MODELS_PARAMETER, IdmCicHttpServer.Instance.RestrictToExposedModels);
            bool autoStartServer = parametersManager.GetParameter(AUTO_START_SERVER_PARAMETER, false);
            
            // Initialise the server singleton
            IdmCicHttpServer.Instance.Port = listeningPort;
            IdmCicHttpServer.Instance.ModelsSessionsLifeTime = sessionsLifetime;
            IdmCicHttpServer.Instance.ExposedIdmFiles.FromDictionary(exposedIdmFiles);
            IdmCicHttpServer.Instance.RestrictToExposedModels = restrictToExposedModels;

            if (autoStartServer)
            {
                // Start the HTTP server automatically
                IdmCicHttpServer.Instance.Start();
                autoStartCheckbox.IsChecked = true;
            }
            
            // Ensure controls states are consistent according to the server status.
            setControlsStateFromServerStatus();

            // Set last session height and width of the window
            Width = parametersManager.GetParameter(LAST_WIDTH_PARAMETER, Width);
            Height = parametersManager.GetParameter(LAST_HEIGHT_PARAMETER, Height);
            WindowState = parametersManager.GetParameter(LAST_WINDOW_STATE, WindowState.Normal);

        }

        /// <summary>
        /// Override the close method to ensure the HTTP server is stopped and store the window size for next opening.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            if (IdmCicHttpServer.Instance.IsStarted())
            {
                // Stop the HTTP server
                IdmCicHttpServer.Instance.Stop(true);
            }

            setApplicationParameter(AUTO_START_SERVER_PARAMETER, autoStartCheckbox.IsChecked.ToString());
            setApplicationParameter(SERVER_PORT_PARAMETER, IdmCicHttpServer.Instance.Port.ToString());
            setApplicationParameter(SESSIONS_LIFETIME_PARAMETER, IdmCicHttpServer.Instance.ModelsSessionsLifeTime.ToString());
            setApplicationParameter(RESTRICT_TO_EXPOSED_MODELS_PARAMETER, IdmCicHttpServer.Instance.RestrictToExposedModels.ToString());
            setApplicationParameter(EXPOSED_IDM_FILES_PARAMETER, JsonConvert.SerializeObject(IdmCicHttpServer.Instance.ExposedIdmFiles.ToDictionary()));

            setApplicationParameter(LAST_WIDTH_PARAMETER, Width.ToString());
            setApplicationParameter(LAST_HEIGHT_PARAMETER, Height.ToString());
            setApplicationParameter(LAST_WINDOW_STATE, WindowState.ToString());

            base.OnClosed(e);
        }

        #endregion

        #region Tools

        /// <summary>
        /// Set application parameter in the IDM-CIC parameters manager, creating it if not exists.
        /// </summary>
        /// <param name="parameterName">Parameter name</param>
        /// <param name="value">Paramater value</param>
        private void setApplicationParameter(string parameterName, string value)
        {
            if (!parametersManager.ParameterExists(parameterName))
            {
                parametersManager.AddParameter(parameterName, value);
            }
            else
            {
                parametersManager.SetParameter(parameterName, value);
            }
        }

        /// <summary>
        /// Set some window controls stats according to the IDM-CIC HTTP server status.
        /// Disable start button, restart button and server port textbox if server is started.
        /// Disable the stop button is the server is not started.
        /// </summary>
        private void setControlsStateFromServerStatus()
        {
            startButton.IsEnabled = !IdmCicHttpServer.Instance.IsStarted();
            restartButton.IsEnabled = IdmCicHttpServer.Instance.IsStarted();
            listeningPortTextbox.IsEnabled = !IdmCicHttpServer.Instance.IsStarted();
            restrictToExposedModelsCheckbox.IsChecked = IdmCicHttpServer.Instance.RestrictToExposedModels;
            stopButton.IsEnabled = IdmCicHttpServer.Instance.IsStarted();
        }
        
        #endregion

        #region Controls events handling

        /// <summary>
        /// Handle the click event on the start button to start the IDM-CIC HTTP server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IdmCicHttpServer.Instance.Start();
            }
            catch (Exception exc)
            {
                AddException(exc);
                MessageBox.Show("Could not start the HTTP server on port " + IdmCicHttpServer.Instance.Port + " (see application logs for more information)",
                        "Start HTTP server", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            setControlsStateFromServerStatus();
        }

        /// <summary>
        /// Handle the click event on the restart button to stop and restart the IDM-CIC HTTP server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restartButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IdmCicHttpServer.Instance.Stop();
                IdmCicHttpServer.Instance.Start();
            }
            catch (Exception exc)
            {
                AddException(exc);
                MessageBox.Show("Could not restart the HTTP server on port " + IdmCicHttpServer.Instance.Port + " (see application logs for more information)",
                        "Restart HTTP server", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
            setControlsStateFromServerStatus();
        }

        /// <summary>
        /// Handle the click event on the stop button to stop the IDM-CIC HTTP server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IdmCicHttpServer.Instance.Stop();
            }
            catch (Exception exc)
            {
                AddException(exc);
                MessageBox.Show("Could not stop the HTTP server (see application logs for more information)",
                        "Stop HTTP server", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            setControlsStateFromServerStatus();
        }


        /// <summary>
        /// Handle the click event of the load default study button to open a model as default model exposed by the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadDefaultStudyButton_Click(object sender, RoutedEventArgs e)
        {
            // Show a dialog to select the IDM file to use as default study
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Select IDM files to use for default study";
            dialog.Filter = "IDM files|*.idm";
            dialog.Multiselect = false;

            if (dialog.ShowDialog() == true)
            {
                Mouse.OverrideCursor = Cursors.Wait;

                try
                {
                    IdmCicHttpServer.Instance.DefaultStudy = new IdmCic.API.Model.IdmStudy(dialog.FileName);
                    defaultStudyPath.Text = dialog.FileName;
                    unloadDefaultStudyButton.IsEnabled = true;
                    Mouse.OverrideCursor = null;
                }
                catch (Exception exc)
                {
                    Mouse.OverrideCursor = null;
                    AddException(exc);
                    // Show a warning for files that are already exposed.
                    MessageBox.Show("The selected file could not be loaded (see application logs for more information)",
                        "Load default IDM-CIC study", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Handle the click event of the unload default study button to 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void unloadDefaultStudyButton_Click(object sender, RoutedEventArgs e)
        {
            IdmCicHttpServer.Instance.DefaultStudy = null;
            defaultStudyPath.Text = "No study loaded";
            unloadDefaultStudyButton.IsEnabled = false;
        }

        /// <summary>
        /// Handle the click event of the add exposed file button to add one or more new exposed files by 
        /// the IDM-CIC HTTP server and update the parameter value within the IDM-CIC parameters manager.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addExposedFileButton_Click(object sender, RoutedEventArgs e)
        {
            // Show a dialog to select IDM files
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Select IDM files to use";
            dialog.Filter = "IDM files|*.idm";
            dialog.Multiselect = true;

            if (dialog.ShowDialog() == true)
            {
                List<string> alreadyDefinedFiles = new List<string>();
                List<string> studiesErrors = new List<string>();

                // Loop on all specified files
                foreach (string filePath in dialog.FileNames)
                {
                    if (IdmCicHttpServer.Instance.ExposedIdmFiles.Contains(filePath))
                    {
                        alreadyDefinedFiles.Add(" - " + Path.GetFileName(filePath));

                        AddException(new FileLoadException("File already exposed", filePath));
                    }
                    else
                    {
                        // Try to load the model to ensure its consistency
                        IdmStudy testStudy = null;

                        try
                        {
                            testStudy = new IdmStudy(filePath);
                        }
                        catch (Exception exc)
                        {
                            studiesErrors.Add(filePath);
                            AddException(exc);
                        }

                        if (testStudy != null)
                        {
                            // The study could be loaded. Add as exposed file to the server
                            string customFileName = Path.GetFileNameWithoutExtension(filePath);
                            IdmCicHttpServer.Instance.ExposedIdmFiles.Add(new ExposedFileData(filePath, customFileName));

                            testStudy = null;
                        }
                    }
                }

                if (studiesErrors.Count > 0)
                {
                    // Show a warning for files that are already exposed.
                    MessageBox.Show("Could not load IDM-CIC models for these files (see application logs for more information):" + Environment.NewLine +
                        string.Join(Environment.NewLine, studiesErrors),
                        "Add exposed file(s)", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                if (alreadyDefinedFiles.Count > 0)
                {
                    // Show a warning for files that are already exposed.
                    MessageBox.Show("These files are already exposed by the server:" + Environment.NewLine + 
                        string.Join(Environment.NewLine, alreadyDefinedFiles),
                        "Add exposed file(s)", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        
        /// <summary>
        /// Handle the click event of the add model session button to open a model session on the server side.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createSessionButton_Click(object sender, RoutedEventArgs e)
        {
            // Show a dialog to select IDM files
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Select IDM files to open";
            dialog.Filter = "IDM files|*.idm";
            dialog.Multiselect = true;

            if (dialog.ShowDialog() == true)
            {
                List<string> notLoadedFiles = new List<string>();
                
                Mouse.OverrideCursor = Cursors.Wait;

                foreach (string filePath in dialog.FileNames)
                {
                    try
                    {
                        IdmCicHttpServer.Instance.LoadIdmModel(filePath, false);
                    }
                    catch (Exception exc)
                    {
                        notLoadedFiles.Add(filePath);
                        AddException(exc);
                    }
                }

                Mouse.OverrideCursor = null;

                if (notLoadedFiles.Count > 0)
                {
                    // Show a warning for files not loaded.
                    MessageBox.Show("Could not load IDM-CIC models for these files (see application logs for more information):" + Environment.NewLine +
                        string.Join(Environment.NewLine, notLoadedFiles),
                        "Add model session(s)", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Handle the checked event of the restrict to exposed models checkbox to restrict the server only to exposed models.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restrictToExposedModelsCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            IdmCicHttpServer.Instance.RestrictToExposedModels = true;
        }

        /// <summary>
        /// Handle the unchecked event of the restrict to exposed models checkbox to restrict the server only to exposed models.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restrictToExposedModelsCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            IdmCicHttpServer.Instance.RestrictToExposedModels = false;
        }

        #endregion

        #region Exceptions logging helper

        /// <summary>
        /// Stores the count of application exception retrieved by the AddException method.
        /// </summary>
        private int exceptionsCount = 0;

        /// <summary>
        /// Default border brush of errors tabs header text to be restored when errors tab is selected.
        /// </summary>
        Brush defaultErrorsHeaderBrush = null;

        /// <summary>
        /// Adds a new exception to the errors log file and to errors logs text box.
        /// </summary>
        /// <param name="e">The exception to log and display in the errors tab</param>
        public void AddException(Exception e)
        {
            // First, log the exception
            LogManager.GetLogger("ApplicationErrors").Error(e, "Application error occured");

            // Get actual date time to be used in the logged message
            DateTime now = DateTime.Now;

            // Increment exceptions count
            exceptionsCount++;

            // Change the errors tabs header text with the new exceptions count
            errorsTabHeader_textBlock.Text = "Errors (" + exceptionsCount.ToString() + ")";

            if (tabs.SelectedItem != errorsTab)
            {
                // Set the text color to red (only if not the selected tab)
                errorsTabHeader_textBlock.Foreground = new SolidColorBrush(Colors.Red);
            }
            
            // Get the stack trace with inner exceptions.
            string stack = e.StackTrace;

            Exception inner = e.InnerException;

            while (inner != null)
            {
                stack += "Inner exception: " + inner.Message + Environment.NewLine + inner.StackTrace;
                inner = inner.InnerException;
            }

            // Get the actual text and adds a separator to allow user to visualise different errors
            string previousText = "";

            if (!string.IsNullOrEmpty(errorsViewer_textBox.Text))
            {
                previousText = Environment.NewLine + "------------------------------" + Environment.NewLine + errorsViewer_textBox.Text;
            }

            // Get the new error text
            string newErrorText = string.Format("{0} {1} => [{2}] : {3}" + Environment.NewLine + "{4}", new string[] {
                now.ToShortDateString(),
                now.ToShortTimeString(),
                e.GetType().FullName,
                e.Message,
                stack});

            // Set the errors textbox new text
            errorsViewer_textBox.Text = newErrorText + previousText;
        }

        /// <summary>
        /// When the application errors tab is selected, ensures the header text color returns to the default color.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabs_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Contains(errorsTab))
            {
                errorsTabHeader_textBlock.Foreground = defaultErrorsHeaderBrush;
            }
        }
        #endregion
    }
}
