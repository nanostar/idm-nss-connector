# IdmCicRestApi
This project was generated with [Visual Studio 2015](https://visualstudio.microsoft.com) version 14.0.25431.01 Update 3.

The project requires the [.NET Framework](https://dotnet.microsoft.com/) version 4.5 to be installed.

To run the solution, you need to have the IDM-CIC application installed. The minimal version is 3.3.3.1.

## Development

Open the solution (IdmCicRestApi.sln).

The solution is linked to the installation folder of the IDM-CIC application (%appdata%\Clever-Age\IDM-CIC).

If the IDM-CIC was installed in another folder, you will have to change the IdmCicRestApi.csproj file to match the installation folder.
Search all matches of the "$(AppData)\Clever-age\IDM-CIC\" text and replace it with the installation folder.

Run the solution in Debug configuration.
In this configuration, all needed dependencies are copied to the output folder and the application can run.

The solution is linked with the "IdmCicRestApi.sln.licenseheader" file. This file can be used with the "License Header manager" extension for Visual Studio to automatically generate license headers in code files.

## Build

Build the solution in Release.
In this configuration, all dependencies of IDM-CIC are not copied to the output folder so it won't run as it.
This is performed because the csproj file of the project has a special directive that embed all dependencies (except IDM-CIC's) within a unique executable file.
Thus, the project can be deployed with two single files, the executable and the NLog configuration file.

After the solution is built in Release configuration, these files are copied in the bin folder under the root folder (../../bin/server) using build events.

## Deployment

To deploy the solution, just copy the two output files in the IDM-CIC installation folder and run the IdmCicRestApi.exe executable.

## Running unit tests

Unit tests can be run in any configuration. All dependencies are copied to the output folder.
In the solution, use the visual studio engine to run all unit tests.
