@echo off

echo "Generating API documentation..."

RD /S /Q "../../../doc/Technical_documentation/files"
"C:\Program Files\doxygen\bin\doxygen.exe" idmcic_restapi.doxy > generation_results.txt 2> generation_errors.txt

pause