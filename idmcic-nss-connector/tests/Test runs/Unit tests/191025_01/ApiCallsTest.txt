Test Name:	TestSessionsLifeTime
Test FullName:	UnitTests.ApiCallsTest.TestSessionsLifeTime
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 193
Test Outcome:	Passed
Test Duration:	0:00:24,516609


Test Name:	TestIdmFilesList
Test FullName:	UnitTests.ApiCallsTest.TestIdmFilesList
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 109
Test Outcome:	Passed
Test Duration:	0:00:01,2469739


Test Name:	TestLoadModel
Test FullName:	UnitTests.ApiCallsTest.TestLoadModel
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 129
Test Outcome:	Passed
Test Duration:	0:00:04,8566541


Test Name:	TestLoadModelRestricted
Test FullName:	UnitTests.ApiCallsTest.TestLoadModelRestricted
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 164
Test Outcome:	Passed
Test Duration:	0:00:04,0282795


Test Name:	TestDefaultStudy
Test FullName:	UnitTests.ApiCallsTest.TestDefaultStudy
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 240
Test Outcome:	Passed
Test Duration:	0:00:36,752999


Test Name:	TestGetObject
Test FullName:	UnitTests.ApiCallsTest.TestGetObject
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 290
Test Outcome:	Passed
Test Duration:	0:00:03,7861478


Test Name:	TestGetObjectFullSerialization
Test FullName:	UnitTests.ApiCallsTest.TestGetObjectFullSerialization
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 367
Test Outcome:	Passed
Test Duration:	0:00:09,7438009


Test Name:	TestGetMass
Test FullName:	UnitTests.ApiCallsTest.TestGetMass
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 445
Test Outcome:	Passed
Test Duration:	0:00:04,128958


Test Name:	TestGetCog
Test FullName:	UnitTests.ApiCallsTest.TestGetCog
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 458
Test Outcome:	Passed
Test Duration:	0:00:12,3010714


Test Name:	TestGetInertiaMatrix
Test FullName:	UnitTests.ApiCallsTest.TestGetInertiaMatrix
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 471
Test Outcome:	Passed
Test Duration:	0:00:35,9413194


Test Name:	TestGetConsumedPower
Test FullName:	UnitTests.ApiCallsTest.TestGetConsumedPower
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 662
Test Outcome:	Passed
Test Duration:	0:00:04,0323171


Test Name:	TestGetDissipatedPower
Test FullName:	UnitTests.ApiCallsTest.TestGetDissipatedPower
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 675
Test Outcome:	Passed
Test Duration:	0:00:03,9978367


Test Name:	TestGetDutyCycleConsumedPower
Test FullName:	UnitTests.ApiCallsTest.TestGetDutyCycleConsumedPower
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 688
Test Outcome:	Passed
Test Duration:	0:00:03,9970651


Test Name:	TestGetDutyCycleDissipatedPower
Test FullName:	UnitTests.ApiCallsTest.TestGetDutyCycleDissipatedPower
Test Source:	D:\works\projets\CNES\idmcic-nss-connector\src\IdmCicRestApi\UnitTests\ApiCallsTest.cs : line 701
Test Outcome:	Passed
Test Duration:	0:00:03,920383